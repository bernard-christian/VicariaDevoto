﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface ITareasRepository
    {
        IEnumerable<Tarea> Tareas { get; }
        void GrabarTarea(Tarea tarea);
        Tarea BorrarTarea(int tareaID);
    }
}
