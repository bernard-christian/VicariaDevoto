﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IProvinciasRepository
    {
        IEnumerable<Provincia> Provincias { get; }
        void GrabarProvincia(Provincia provincia);
        Provincia BorrarProvincia(int provinciaID);
    }
}
