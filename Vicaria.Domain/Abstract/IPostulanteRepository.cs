﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IPostulanteRepository
    {
        IEnumerable<Postulante> Postulantes { get; }
        
        void GrabarPostulante(Postulante postulante);

        Postulante BorrarPostulante(int postulanteID);

      
       
        IEnumerable<EstadoCivil> EstadosCiviles { get; }
        IEnumerable<TipoDeDocumento> TiposDeDocumentos { get; }
        IEnumerable<Nacionalidad> Nacionalidades { get; }
        IEnumerable<EstudioCursado> Estudios { get; }
        IEnumerable<Provincia> Provincias { get; }
        IEnumerable<GrupoFamiliar> GruposFamiliares { get; }
    }
}
