﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IJornadasLaboralesRepository
    {
        IEnumerable<JornadaLaboral> JornadasLaborales { get; }
        void GrabarJornadaLaboral(JornadaLaboral jornadaLaboral);
        JornadaLaboral BorrarJornadaLaboral(int jornadaLaboralID);
        string GetJornada(int id);
    }
}
