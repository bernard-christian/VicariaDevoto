﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IEmpleadorRepository
    {
        IEnumerable<Empleador> Empleadores { get; }

        void GrabarEmpleador(Empleador empleador);

        Empleador BorrarEmpleador(int empleadorID); 

        IEnumerable<TipoDeDocumento> TiposDeDocumentos { get; }
        IEnumerable<Nacionalidad> Nacionalidades { get; }
        IEnumerable<Provincia> Provincias { get; }
    }
}
