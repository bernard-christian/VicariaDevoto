﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IDocumentosRepository
    {

        IEnumerable<TipoDeDocumento> TiposDeDocumentos { get; }
        void GrabarDocumento(TipoDeDocumento tipoDeDocumento);
        TipoDeDocumento BorrarDocumento(int ID);


    }
}
