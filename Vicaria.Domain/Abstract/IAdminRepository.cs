﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IAdminRepository
    {
        IEnumerable<EstadoCivil> EstadosCiviles { get; }
        void GrabarEstadoCivil(EstadoCivil estadoCivil);
        EstadoCivil BorrarEstadoCivil(int estadoCivilID);
       

        //IEnumerable<TipoDeDocumento> TiposDeDocumentos { get; }
        //void GrabarTipoDeDocumento(TipoDeDocumento tipoDeDocumento);
        //EstadoCivil BorrarTipoDeDocumento(int ID);

    }
}
