﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IContratacionesRepository
    {
        IEnumerable<Contratacion> Contrataciones { get; }
        void GrabarContratacion(Contratacion contratacion);
        Contratacion BorrarContratacion(int contratacionID);
    }
}
