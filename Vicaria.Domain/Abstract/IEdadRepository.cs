﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IEdadRepository
    {
        IEnumerable<Edad> Edades { get; }
        void GrabarEdad(Edad edad);
        Edad BorrarEdad(int EdadID);
    }
}
