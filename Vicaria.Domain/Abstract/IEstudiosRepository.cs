﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IEstudiosRepository
    {
        IEnumerable<EstudioCursado> EstudiosCursados { get; }
        void GrabarEstudioCursado(EstudioCursado estudioCursado);
        EstudioCursado BorrarEstudioCursado(int estudioCursadoID);
    }
}
