﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;
using System.Data.Entity;

namespace Vicaria.Domain.Abstract
{
    public interface IOfertasRepository
    {
        IQueryable<Oferta> Ofertas { get; }
        void GrabarOferta(Oferta oferta); //, string[] TareasSeleccionadas
        Oferta BorrarOferta(int OfertaID);
        DbSet<Oferta> GetOfertas { get; }//no lo uso
        Oferta GetOferta(int id);
        string GetCategoriaDescripcion(int id);
        string GetJornadaDescripcion(int id);
        //HashSet<int> TareasEnOferta(Oferta oferta);
        //DbSet<Tarea> GetTareas { get; }
        void GrabarEntrevista(Entrevista entrevista);
        EstudioCursado getEstudioCursado(int idEstudioCursado);

        Entrevista GetEntrevista(int idOferta, int idBusqueda);

        Empleador getEmpleador(int idEmpleador);
        Busqueda GetBusqueda(int idBusueda);
        Postulante getPostulante(int idPostulante);
        Edad getEdad(int idEdad);
        Categoria getCategoria(int idCategoria);
        JornadaLaboral getJornadaLaboral(int idJornadaLaboral);
        Contratacion getContratacion(int idContratacion);

        IEnumerable<Categoria> Categorias { get; }
        IEnumerable<Contratacion> Contrataciones { get; }
        IEnumerable<JornadaLaboral> JornadasLaborales { get; }
        IEnumerable<Tarea> Tareas { get; }
        //IEnumerable<Edad> Edades { get; }
        IEnumerable<EstudioCursado> EstudiosCursados { get; }
        IEnumerable<Busqueda> Busquedas { get; }
        IEnumerable<Postulante> Postulantes { get; }
    }
}
