﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface ICategoriasRepository
    {
        IEnumerable<Categoria> Categorias { get; }
        void GrabarCategoria(Categoria categoria);
        Categoria BorrarCategoria(int CategoriaID);
        string GetCategoria(int id);
    }
}
