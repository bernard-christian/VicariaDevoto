﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IEntrevistaRepository
    {
        IQueryable<Entrevista> Entrevistas { get; }
        void GrabarEntrevista(Entrevista entrevista);
        void GrabarOferta(Oferta oferta);
        void GrabarBusqueda(Busqueda busqueda);
        Entrevista BorrarEntrevista(int entrevistaID);
        Oferta GetOferta(int id);
        Busqueda GetBusqueda(int idBusqueda);
    }
}
