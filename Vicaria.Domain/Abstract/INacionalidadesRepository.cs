﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface INacionalidadesRepository
    {
        IEnumerable<Nacionalidad> Nacionalidades { get; }
        void GrabarNacionalidad(Nacionalidad nacionalidad);
        Nacionalidad BorrarNacionalidad(int nacionalidadID);
    }
}
