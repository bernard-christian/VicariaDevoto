﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IBusquedasRepository
    {
        IEnumerable<Busqueda> Busquedas { get; }
        void GrabarBusqueda(Busqueda busqueda);
        Busqueda BorrarBusqueda(int BusquedaID);
        Postulante getPostulante(int idPostulante);
        Categoria getCategoria(int idCategoria);
        JornadaLaboral getJornadaLaboral(int idJornadaLaboral);

        IEnumerable<Categoria> Categorias { get; }
        IEnumerable<JornadaLaboral> JornadasLaborales { get; }
    }
}
