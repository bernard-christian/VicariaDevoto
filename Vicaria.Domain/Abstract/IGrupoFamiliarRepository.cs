﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Abstract
{
    public interface IGrupoFamiliarRepository
    {
        IEnumerable<GrupoFamiliar> GruposFamiliares { get; }
        void GrabarGrupoFamiliar(GrupoFamiliar grupoFamiliar);
        GrupoFamiliar BorrarGrupoFamiliar(int grupoFamiliarId);
    }
}
