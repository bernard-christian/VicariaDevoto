﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Vicaria.Domain.Entities
{
    public class Provincia
    {
        [HiddenInput(DisplayValue = false)]
        public int ProvinciaId { get; set; }
        [Required(ErrorMessage = "Debe ingresar un nombre")]
        [Display(Name = "Provincia")]
        public string Nombre { get; set; }
    }
}
