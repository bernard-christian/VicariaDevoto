﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Vicaria.Domain.Entities
{
    public class Busqueda
    {
        public Busqueda() 
        {
            
        }

        public int BusquedaID { get; set; }

        
       

        [Column("Postulante_PostulanteID")]
        [ ForeignKey("Postulante")]
        public int PostulanteID { get; set; }
        public virtual Postulante Postulante { get; set; }

        [Required(ErrorMessage = "Debe ingresar la categoría de la búsqueda")]
        [Column("Categoria_CategoriaID")]
        public int CategoriaID { get; set; }
        //[Required(ErrorMessage = "Debe ingresar la categoría de la búsqueda")]
        public virtual Categoria Categoria { get; set; }

        [Required(ErrorMessage = "Debe ingresar la jornada laboral")]
        [Column("JornadaLaboral_JornadaLaboralID")]
        public int JornadaLaboralID { get; set; }

        [Display(Name = "Jornada Laboral")]
        //[Required(ErrorMessage = "Debe ingresar la jornada laboral")]
        public virtual JornadaLaboral JornadaLaboral { get; set; }

        public DateTime BuscaDesde { get; set; }

        public bool BusquedaActiva { get;  set; }

        
    }
}
