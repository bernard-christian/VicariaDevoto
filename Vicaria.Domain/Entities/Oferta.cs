﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Vicaria.Domain.Entities
{
    public class Oferta
    {
        [HiddenInput(DisplayValue = false)]
        public int OfertaID { get; set; }
        public virtual ICollection<Entrevista> Entrevistas { get; set; }

        [ForeignKey("Empleador")]
        [Column("Empleador_EmpleadorID")]
        public int EmpleadorID { get; set; }
        public virtual Empleador Empleador { get; set; }

        [Required(ErrorMessage = "Debe ingresar una vacante")]
        public int Vacantes { get; set; }

        [Required(ErrorMessage = "Debe ingresar una categoria")]
        [Column("Categoria_CategoriaID")]
        public int CategoriaID { get; set; }
        public virtual Categoria Categoria { get; set; }

        [Required(ErrorMessage = "Debe ingresar una jornada laboral")]
        [Column("JornadaLaboral_JornadaLaboralID")]
        public int JornadaLaboralID { get; set; }
        [Display(Name = "Jornada Laboral")]
        public virtual JornadaLaboral JornadaLaboral { get; set; }

        [Required(ErrorMessage = "Debe ingresar una tarea")]
        //public virtual ICollection<Tarea> Tareas { get; set; }
        public string Tarea { get; set; }

        [Display(Name = "Modo de Contratacion")]
        [Required(ErrorMessage = "Debe ingresar una Modalidad de contratación")]
        [Column("Contratacion_ContratacionID")]
        public int ContratacionID { get; set; }
        //public virtual ICollection<Contratacion> Contratacion { get; set; }
        public virtual Contratacion Contratacion { get; set; }

        [Required(ErrorMessage = "Debe ingresar una descripción")]
        public string Descripcion { get; set; }


        [Display(Name = "Edad Mínima")]
        [Required(ErrorMessage = "Debe ingresar una de edad")]
        [Column("EdadMin")]
        public int EdadMin { get; set; }

        [Display(Name = "Edad Máxima")]
        [Required(ErrorMessage = "Debe ingresar una de edad")]
        [Column("EdadMax")]
        public int EdadMax { get; set; }

        [Display(Name = "Nivel de Estudios")]
        [Required(ErrorMessage = "Debe ingresar el  nivel de estudios solicitado")]
        [Column("EstudioCursado_EstudioCursadoID")]
        public int EstudioCursadoID { get; set; }
        public virtual EstudioCursado EstudioCursado { get; set; }

        [Required(ErrorMessage = "Debe ingresar el sexo deseado para el puesto")]
        [Display(Name = "Sexo")]
        public string Sexo { get; set; }

        [Required(ErrorMessage = "Debe ingresar el salario ofrecido")]
        public int Salario { get; set; }

        [Required(ErrorMessage = "Debe ingresar el lugar de trabajo")]
        public string LugarDeTrabajo { get; set; }

        [Required(ErrorMessage = "Debe ingresar el horario")]
        public string Horario { get; set; }

        [Required(ErrorMessage = "Debe ingresar el contacto")]
        public string Contacto { get; set; }

        [RegularExpression(@"^[0-9]{0,15}$", ErrorMessage = "Solo puede cargar números")]
        public string Celular { get; set; }

        [Required(ErrorMessage = "Debe ingresar una observación")]
        public string Observacion { get; set; }

        [Required(ErrorMessage = "Debe ingresar un estad0 de la oferta")]
        public bool OfertaActiva { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime OfertaDesde { get; set; }

    }
}
