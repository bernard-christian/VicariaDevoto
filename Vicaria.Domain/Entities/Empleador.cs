﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Vicaria.Domain.Entities
{
    public class Empleador
    {
        //public Empleador()
        //{
        //    //this.Oferta = new List<Oferta>(); para listar las ofertas de trabajo
        //}

        [HiddenInput(DisplayValue = false)]
        public int EmpleadorID { get; set; }

        public virtual ICollection<Oferta> Ofertas { get; set; }
       

        [Required(ErrorMessage = "Por favor ingrese el nombre del empleador")]
        public string Nombre { get; set; }
        
        [Required(ErrorMessage = "Por favor ingrese el apellido del empleador")]
        public string Apellido { get; set; }
        
        [Required(ErrorMessage = "Por favor ingrese el solicitante de la oferta (Particular o nombre de empresa)")]
        public string Solicitante { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el tipo de documento del empleador")]
        [Column("TipoDeDocumento_TipoDeDocumentoID")]
        public int TipoDeDocumentoID { get; set; }
        public virtual TipoDeDocumento TipoDeDocumento { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el documento del empleador")]
        [Display(Name = "Número de Documento")]
        public int Documento { get; set; }

        [Display(Name = "Razón Social")]
        public string RazonSocial { get; set; }

        [Required(ErrorMessage = "Por favor ingrese la dirección del empleador")]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "Por favor ingrese la localidad del empleador")]
        public string Localidad { get; set; }

        [Required(ErrorMessage = "Por favor ingrese la provincia de nacimiento del empleador")]
        [Column("Provincia_ProvinciaID")]
        [Display(Name = "Provincia")]
        public int ProvinciaID { get; set; }
        public virtual Provincia Provincia { get; set; }

        //[Required(ErrorMessage = "Por favor ingrese el número de teléfono del empleador")]
        [RegularExpression(@"^[0-9-]{0,15}$", ErrorMessage = "Solo puede cargar números y guiones medios")]
        public string Telefono { get; set; }

       [RegularExpression(@"^[0-9-]{0,15}$", ErrorMessage = "Solo puede cargar números y guiones medios")]
        public string Celular { get; set; }

        //[Required(ErrorMessage = "Por favor ingrese el mail del empleador")]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "El mail no tiene un formato apropiado")]

        public string Mail { get; set; }



        //[Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime FechaAlta { get; set; }

        //[Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime keepAlive { get; set; }

        

    }
}
