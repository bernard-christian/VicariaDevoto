﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Vicaria.Domain.Entities
{
    public class Edad

    {
        [HiddenInput(DisplayValue = false)]
        public int EdadID { get; set; }
        [Required(ErrorMessage = "Debe ingresar una edad desde")]
        public int Desde { get; set; }

        public int Hasta { get; set; }

        public string Rango { get; set; }
    }
}
