﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Vicaria.Domain.Entities
{
    public class EstudioCursado
    {
        [HiddenInput(DisplayValue = false)]
        public int EstudioCursadoID { get; set; }
        [Required(ErrorMessage = "Debe ingresar una descripción")]
        public string Descripcion { get; set; }
    }
}
