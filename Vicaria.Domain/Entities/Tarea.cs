﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Vicaria.Domain.Entities
{
    public class Tarea

    {
        [HiddenInput(DisplayValue = false)]
        public int TareaID { get; set; }
        [Required(ErrorMessage = "Debe ingresar una tarea")]
        public string Descripcion { get; set; }
       // public virtual ICollection<Oferta> Ofertas { get; set; }
    }
}
