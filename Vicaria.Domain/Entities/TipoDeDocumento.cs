﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Vicaria.Domain.Entities
{
    public class TipoDeDocumento
    {
        [HiddenInput(DisplayValue = false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TipoDeDocumentoID { get; set; }
        [Required(ErrorMessage = "Debe ingresar una descripción")]
        [Display(Name = "Tipo de documento")]
        public string Descripcion { get; set; }
    }
}
