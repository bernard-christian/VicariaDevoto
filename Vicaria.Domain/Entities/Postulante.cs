﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;


namespace Vicaria.Domain.Entities
{
    public class Postulante
    {
        public Postulante()
        {
            this.Busquedas = new List<Busqueda>();
        }
        
        [HiddenInput(DisplayValue = false)]
        public int PostulanteID { get; set; }


        public virtual ICollection<Busqueda> Busquedas { get; set; }
        public virtual ICollection<Entrevista> Entrevistas { get; set; }


        [Required(ErrorMessage = "Debe  ingresar el nombre del postulante")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Debe  ingresar el apellido del postulante")]
        public string Apellido { get; set; }

        
        [Display(Name = "Tipo de Documento")]
        public virtual TipoDeDocumento TipoDeDocumento { get; set; }
        [Required(ErrorMessage = "Debe  ingresar el tipo de documento del postulante")]
        [Column("TipoDeDocumento_TipoDeDocumentoID")]
        public int TipoDeDocumentoID { get; set; }
        
        [Required(ErrorMessage = "Debe  ingresar el documento del postulante")]
        [Display(Name = "Número de Documento")]
        public  int Documento { get; set; }

        [Required(ErrorMessage = "Debe  ingresar el sexo del postulante")]
        [Display(Name = "Sexo")]
        public string sexo { get; set; }

        [Required(ErrorMessage = "Debe  ingresar la fecha de nacimienito del postulante")]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Nacimiento")]
        public DateTime FechaDeNacimiento { get; set; }

        [Required(ErrorMessage = "Debe ingresar la nacionalidad del postulante")]
        [Column("Nacionalidad_NacionalidadID")]
        [Display(Name = "Nacionalidad")]
        public int NacionalidadID { get; set; }
        public virtual Nacionalidad Nacionalidad { get; set; }

        [Required(ErrorMessage = "Debe ingresar el estado civil del postulante")]
        [Column("EstadoCivil_EstadoCivilID")]
        public int EstadoCivilID { get; set; }
        [Display(Name = "Estado Civil")]
        public virtual EstadoCivil EstadoCivil { get; set; }

        [Required(ErrorMessage = "Debe ingresar la dirección del postulante")]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "Debe ingresar la localidad del postulante")]
        public string Localidad { get; set; }

        [Required(ErrorMessage = "Debe ingresar la provincia de nacimiento del postulante")]
        [Column("Provincia_ProvinciaID")]
        [Display(Name = "Provincia")]
        public int ProvinciaID { get; set; }
        public virtual Provincia Provincia { get; set; }

        //[Required(ErrorMessage = "Debe ingresar el teléfono del postulante")]
        [RegularExpression(@"^[0-9-]{0,15}$", ErrorMessage = "Solo puede cargar números y guiones medios")]
        public string Telefono { get; set; }

        [RegularExpression(@"^[0-9-]{0,15}$", ErrorMessage = "Solo puede cargar números y guiones medios")]
        public string Celular { get; set; }

        //[Required(ErrorMessage = "Por favor ingrese el mail del postulante")]
        //[EmailAddress]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "El mail no tiene un formato apropiado")]
        public string Mail { get; set; }

        [Required(ErrorMessage = "Debe ingresar el grupo familiar conviviente del postulante")]
        [Column("GrupoFamiliar_GrupoFamiliarID")]
        public int GrupoFamiliarID { get; set; }
        [Display(Name = "Grupo Familiar Conviviente")]
        public virtual GrupoFamiliar GrupoFamiliar { get; set; }

        [Required(ErrorMessage = "Debe ingresar la especialización laboral del postulante")]
        [Display(Name = "Especializaión Laboral")]
        public string EspecializacionLaboral { get; set; }

        [Required(ErrorMessage = "Debe ingresar el mayor nivel de estudios cursados del postulante")]
        [Column("EstudioCursado_EstudioCursadoID")]
        public int EstudioCursadoID { get; set; }
        
        [Display(Name = "Estudios Cursados")]
        public virtual EstudioCursado EstudioCursado { get; set; }

        [Required(ErrorMessage = "Debe ingresar el detalle de estudios del postulante")]
        [Display(Name = "Detalle de Estudio")]
        public string DetalleDeEstudio { get; set; }
       
        [Required(ErrorMessage = "Debe ingresar como se enteró el de este servicio postulante")]
        [Display(Name = "¿Cómo se enteró?")]
        public string ComoSeEntero { get; set; }

        [Required(ErrorMessage = "Debe ingresar las referencias del postulante")]
        public string Referencias { get; set; }
        
        [Required(ErrorMessage = "Debe ingresar las observaciones acerca del postulante")]
        public string Observaciones { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime KeepAlive { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime FechaAlta { get; set; }

        [Required(ErrorMessage = "Debe ingresar el título obtenido por el postulante")]
        [Display(Name = "Título")]
        public string Titulo { get; set; }

        public bool EstudiosTerminados { get; set; }

        [Required(ErrorMessage = "Debe ingresar el link al CV el postulante (el de LinkedIn)")]
        public string CV { get; set; }

        [Required(ErrorMessage = "Debe ingresar los conocimientos de idioma del postulante")]
        public string Idioma { get; set; }

        [Required(ErrorMessage = "Debe ingresar los conocimientos de informática del postulante")]
        public string Informatica { get; set; }




    }
}
