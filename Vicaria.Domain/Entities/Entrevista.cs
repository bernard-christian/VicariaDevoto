﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;


namespace Vicaria.Domain.Entities
{
    public class Entrevista
    {
        

        [HiddenInput(DisplayValue = false)]
        public int entrevistaID { get; set; }

        [ForeignKey("Oferta")]
        [Column("Oferta_OfertaID")]
        public int OfertaID { get; set; }
        public virtual Oferta Oferta { get; set; }

        [ForeignKey("Busqueda")]
        [Column("Busqueda_BusquedaID")]
        public int BusquedaID { get; set; }
        public virtual Busqueda Busqueda { get; set; }


        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime fechaAlta { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de la entrvista ")]
        public DateTime fechaEntrevista { get; set; }

        public string Observaciones { get; set; }

        [Display(Name = "Secubre la vacante ")]
        public bool cubreVacante { get; set; }

        [ForeignKey("Postulantes")]
        [Column("Postulantes_Postulante_ID")]
        public int PostulanteId { get; set; }
        public virtual Postulante Postulantes { get; set; }

    }
}
