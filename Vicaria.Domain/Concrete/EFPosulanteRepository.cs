﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using System.Data.Entity.Validation;
using System.Diagnostics;


namespace Vicaria.Domain.Concrete
{
    public class EFPosulanteRepository : IPostulanteRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Postulante> Postulantes
        {
            get { return context.Postulantes.OrderByDescending(p =>p.PostulanteID); }
        }

        public IEnumerable<EstadoCivil> EstadosCiviles
        {
            get { return context.EstadosCiviles; }

        }

        public IEnumerable<TipoDeDocumento> TiposDeDocumentos
        {
            get { return context.TiposDeDocumentos; }

        }

        public IEnumerable<Nacionalidad> Nacionalidades
        {
            get { return context.Nacionalidades; }

        }

        public IEnumerable<EstudioCursado> Estudios
        {
            get { return context.EstudiosCursados; }

        }
        public IEnumerable<Provincia> Provincias
        {
            get { return context.Provincias; }

        }

        public IEnumerable<GrupoFamiliar> GruposFamiliares
        {
            get { return context.GruposFamiliares; }

        }

       

        public void GrabarPostulante(Postulante postulante)
        {
            if (postulante.PostulanteID == 0)
            {
                context.Postulantes.Add(postulante);
            }
            else
	        {
                Postulante dbEntrada = context.Postulantes.Find(postulante.PostulanteID);
                if (dbEntrada != null)
                {
                    dbEntrada.Nombre = postulante.Nombre;
                    dbEntrada.Apellido = postulante.Apellido;
                    dbEntrada.TipoDeDocumentoID = postulante.TipoDeDocumentoID;
                    dbEntrada.Documento = postulante.Documento;
                    dbEntrada.sexo = postulante.sexo;
                    dbEntrada.FechaDeNacimiento = postulante.FechaDeNacimiento;
                    dbEntrada.NacionalidadID = postulante.NacionalidadID;
                    dbEntrada.EstadoCivilID = postulante.EstadoCivilID;
                    dbEntrada.Direccion = postulante.Direccion;
                    dbEntrada.Localidad = postulante.Localidad;
                    dbEntrada.ProvinciaID = postulante.ProvinciaID;
                    dbEntrada.Telefono = postulante.Telefono;
                    dbEntrada.Celular = postulante.Celular;
                    dbEntrada.Mail = postulante.Mail;
                    dbEntrada.GrupoFamiliarID = postulante.GrupoFamiliarID;
                    dbEntrada.EspecializacionLaboral = postulante.EspecializacionLaboral;
                    dbEntrada.EstudioCursadoID = postulante.EstudioCursadoID;
                    dbEntrada.DetalleDeEstudio = postulante.DetalleDeEstudio;
                    dbEntrada.ComoSeEntero = postulante.ComoSeEntero;
                    dbEntrada.Referencias = postulante.Referencias;
                    dbEntrada.Observaciones = postulante.Observaciones;
                    dbEntrada.KeepAlive = postulante.KeepAlive;
                    dbEntrada.FechaAlta = postulante.FechaAlta;
                    dbEntrada.Titulo = postulante.Titulo;
                    dbEntrada.EstudiosTerminados = postulante.EstudiosTerminados;
                    dbEntrada.CV = postulante.CV;
                    dbEntrada.Idioma = postulante.Idioma;
                    dbEntrada.Informatica = postulante.Informatica;
                }
	        }
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }

        public Postulante BorrarPostulante(int postulanteID)
        {
            Postulante postulanteABorrar = context.Postulantes.Find(postulanteID);
            if (postulanteABorrar != null)
            {
                context.Postulantes.Remove(postulanteABorrar);
                context.SaveChanges();
            }
            return postulanteABorrar;
        }

       
    }
}
