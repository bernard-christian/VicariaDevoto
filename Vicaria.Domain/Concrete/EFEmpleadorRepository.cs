﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Vicaria.Domain.Concrete
{
    public class EFEmpleadorRepository : IEmpleadorRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Empleador> Empleadores
        {
            get { return context.Empleadores; }
        }

        

        public IEnumerable<TipoDeDocumento> TiposDeDocumentos
        {
            get { return context.TiposDeDocumentos; }

        }

        public IEnumerable<Nacionalidad> Nacionalidades
        {
            get { return context.Nacionalidades; }

        }

        
        public IEnumerable<Provincia> Provincias
        {
            get { return context.Provincias; }

        }

       

        public void GrabarEmpleador(Empleador empleador)
        {
            if (empleador.EmpleadorID == 0)
            {
                context.Empleadores.Add(empleador);
            }
            else
            {
                Empleador dbEntrada = context.Empleadores.Find(empleador.EmpleadorID);
                if (dbEntrada != null)
                {
                    dbEntrada.Nombre = empleador.Nombre;
                    dbEntrada.Apellido = empleador.Apellido;
                    dbEntrada.Solicitante = empleador.Solicitante;
                    dbEntrada.TipoDeDocumentoID = empleador.TipoDeDocumentoID;
                    dbEntrada.Documento = empleador.Documento;
                    dbEntrada.Direccion = empleador.Direccion;
                    dbEntrada.Localidad = empleador.Localidad;
                    dbEntrada.ProvinciaID = empleador.ProvinciaID;
                    dbEntrada.Telefono = empleador.Telefono;
                    dbEntrada.Celular = empleador.Celular;
                    dbEntrada.Mail = empleador.Mail;
                    dbEntrada.keepAlive = empleador.keepAlive;
                    dbEntrada.RazonSocial = empleador.RazonSocial;

                }
            }
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }

        public Empleador BorrarEmpleador(int empleadorID)
        {
            Empleador empleadorABorrar = context.Empleadores.Find(empleadorID);
            if (empleadorABorrar != null)
            {
                context.Empleadores.Remove(empleadorABorrar);
                context.SaveChanges();
            }
            return empleadorABorrar;
        }
    }
}
