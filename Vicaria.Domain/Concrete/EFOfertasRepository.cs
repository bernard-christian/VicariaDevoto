﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using System.Data.Entity;
using System.Collections;
using System.Collections.ObjectModel;
using System.Data.Entity.Validation;
using System.Diagnostics;


namespace Vicaria.Domain.Concrete
{
    public class EFOfertasRepository : IOfertasRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<Oferta> Ofertas
        {
            get { return context.Ofertas; }

        }

        public DbSet<Oferta> GetOfertas// no la uso
        {
            get { return context.Ofertas; }
        }

        //public DbSet<Tarea> GetTareas
        //{
        //    get { return context.Tareas; }
        //}

        //public HashSet<int> TareasEnOferta(Oferta oferta)
        //{
        //    if (oferta.Tareas == null)
        //    {
        //        return new HashSet<int>();
        //    }
        //    else
        //    {
        //        return new HashSet<int>(oferta.Tareas.Select(t => t.TareaID));
        //    }
            
        //}

        public Oferta GetOferta(int id)
        {
            Oferta oferta = context.Ofertas.Find(id);
            return oferta;
        }

        public string GetCategoriaDescripcion(int id)
        {
            Categoria categoria = context.Categorias.Find(id);
            return categoria.Descripcion;
        }

        public string GetJornadaDescripcion(int id)
        {
            JornadaLaboral jornada = context.JornadasLaborales.Find(id);
            return jornada.Descripcion;
        }

        public Entrevista GetEntrevista(int idOferta, int idBusqueda)
        {
            //Entrevista entrevista = context.Entrevistas.Find(idOferta );

            Entrevista entrevista = context.Entrevistas.FirstOrDefault(e => e.OfertaID == idOferta &&  e.BusquedaID == idBusqueda);

           

            return entrevista;
        }

        public Busqueda GetBusqueda(int idBusqueda)
        {
            Busqueda busqueda = context.Busquedas.Find(idBusqueda);
            return busqueda;
        }

        public IEnumerable<Categoria> Categorias
        {
            get { return context.Categorias; }

        }

        public IEnumerable<JornadaLaboral> JornadasLaborales
        {
            get { return context.JornadasLaborales; }

        }

        public IEnumerable<Contratacion> Contrataciones
        {
            get { return context.Contrataciones; }

        }

        public IEnumerable<Tarea> Tareas
        {
            get { return context.Tareas; }

        }

        public IEnumerable<Edad> Edades
        {
            get { return context.Edades; }

        }

        public IEnumerable<EstudioCursado> EstudiosCursados
        {
            get { return context.EstudiosCursados; }

        }

        public IEnumerable<Busqueda> Busquedas
        {
            get { return context.Busquedas; }

        }

        public IEnumerable<Postulante> Postulantes
        {
            get { return context.Postulantes; }

        }

        public void GrabarEntrevista(Entrevista entrevista)
        {
            if (entrevista.entrevistaID == 0)
            {
                context.Entrevistas.Add(entrevista);
            }
            else
            {
                Entrevista dbEntrada = context.Entrevistas.Find(entrevista.entrevistaID);
                if (dbEntrada != null)
                {
                    dbEntrada.OfertaID = entrevista.OfertaID;
                    dbEntrada.BusquedaID = entrevista.BusquedaID;
                    dbEntrada.fechaAlta = entrevista.fechaAlta;
                    dbEntrada.fechaEntrevista = entrevista.fechaEntrevista;
                    dbEntrada.Observaciones = entrevista.Observaciones;
                    dbEntrada.cubreVacante = entrevista.cubreVacante;

                }
            }
            context.SaveChanges();
        }


        public void GrabarOferta(Oferta oferta) //, string[] TareasSeleccionadas
        {
            if (oferta.OfertaID == 0)
            {
                context.Ofertas.Add(oferta);
            }
            else
            {
                Oferta dbOferta = context.Ofertas.Find(oferta.OfertaID);
            
                ////verifico si hay que actualiar tareas                
                //var ofertaTarea = new HashSet<int>();
                //if(oferta.OfertaID != 0)
                //{
                //    ofertaTarea = new HashSet<int>(dbOferta.Tareas.Select(t => t.TareaID));
                //}
            
            
                //bool hayQueActualizarTareas = false;
                //foreach (var tarea in oferta.Tareas)
                //{
                //    if (!ofertaTarea.Contains(tarea.TareaID))
                //        hayQueActualizarTareas = true;
                //};

                //actualizo los campos necesarios en la base
                //if(hayQueActualizarTareas)
                //    dbOferta.Tareas = oferta.Tareas;
                dbOferta.EmpleadorID = oferta.EmpleadorID;
                dbOferta.Vacantes = oferta.Vacantes;
                dbOferta.CategoriaID = oferta.CategoriaID;
                dbOferta.ContratacionID = oferta.ContratacionID;
                dbOferta.JornadaLaboralID = oferta.JornadaLaboralID;
                dbOferta.Descripcion = oferta.Descripcion;
                //dbOferta.EdadID = oferta.EdadID;
                dbOferta.EdadMin = oferta.EdadMin;
                dbOferta.EdadMax = oferta.EdadMax;
                dbOferta.EstudioCursadoID = oferta.EstudioCursadoID;
                dbOferta.Sexo = oferta.Sexo;
                dbOferta.Salario = oferta.Salario;
                dbOferta.LugarDeTrabajo = oferta.LugarDeTrabajo;
                dbOferta.Horario = oferta.Horario;
                dbOferta.Contacto = oferta.Contacto;
                dbOferta.Celular = oferta.Celular;
                dbOferta.Observacion = oferta.Observacion;
                dbOferta.OfertaActiva = oferta.OfertaActiva;
                dbOferta.OfertaDesde = oferta.OfertaDesde;
                dbOferta.Tarea = oferta.Tarea;

               }
            try
            {
                
                context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
           
        }

        public Oferta BorrarOferta(int id)
        {
            Oferta ofertaABorrar = context.Ofertas.Find(id);
            if (ofertaABorrar != null)
            {
                context.Ofertas.Remove(ofertaABorrar);
                context.SaveChanges();
            }
            return ofertaABorrar;
        }

        public Categoria getCategoria(int idCategoria)
        {
            Categoria categoria = context.Categorias.Find(idCategoria);
            return categoria;
        }

        public Contratacion getContratacion(int idContratacion)
        {
            Contratacion contratacion = context.Contrataciones.Find(idContratacion);
            return contratacion;
        }

        public EstudioCursado getEstudioCursado(int idEstudioCursado)
        {
            EstudioCursado estudio = context.EstudiosCursados.Find(idEstudioCursado);
            return estudio;
        }

        public Empleador getEmpleador(int idEmpleador)
        {
            Empleador empleador = context.Empleadores.Find(idEmpleador);
            return empleador;
        }

        public Edad getEdad(int idEdad)
        {
            Edad edad = context.Edades.Find(idEdad);
            return edad;
        }


        public Postulante getPostulante(int idPostulante)
        {
            Postulante postulante = context.Postulantes.Find(idPostulante);
            return postulante;
        }

        public JornadaLaboral getJornadaLaboral(int idJornadaLaboral)
        {
            JornadaLaboral jornada = context.JornadasLaborales.Find(idJornadaLaboral);
            return jornada;
        }

       
    }
}
