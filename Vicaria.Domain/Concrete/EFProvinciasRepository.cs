﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFProvinciasRepository : IProvinciasRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Provincia> Provincias
        {
            get { return context.Provincias; }

        }

        public void GrabarProvincia(Provincia provincia)
        {
            if (provincia.ProvinciaId == 0)
            {
                context.Provincias.Add(provincia);
            }
            else
            {
                Provincia dbEntrada = context.Provincias.Find(provincia.ProvinciaId);
                if (dbEntrada != null)
                {
                    dbEntrada.Nombre = provincia.Nombre;
                }
            }
            context.SaveChanges();
        }

        public Provincia BorrarProvincia(int id)
        {
            Provincia provinciaABorrar = context.Provincias.Find(id);
            if (provinciaABorrar != null)
            {
                context.Provincias.Remove(provinciaABorrar);
                context.SaveChanges();
            }
            return provinciaABorrar;
        }
    }
}
