﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFAdminRepository : IAdminRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<EstadoCivil> EstadosCiviles
        {
            get { return context.EstadosCiviles; }

        }

        public void GrabarEstadoCivil(EstadoCivil estadoCivil)
        {
            if (estadoCivil.EstadoCivilID == 0)
            {
                context.EstadosCiviles.Add(estadoCivil);
            }
            else
            {
                EstadoCivil dbEntrada = context.EstadosCiviles.Find(estadoCivil.EstadoCivilID);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = estadoCivil.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public EstadoCivil BorrarEstadoCivil(int id)
        {
            EstadoCivil estadoCivilABorrar = context.EstadosCiviles.Find(id);
            if (estadoCivilABorrar != null)
            {
                context.EstadosCiviles.Remove(estadoCivilABorrar);
                context.SaveChanges();
            }
            return estadoCivilABorrar;
        }
    }
}
