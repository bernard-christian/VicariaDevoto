﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFEdadesRepository : IEdadRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Edad> Edades
        {
            get { return context.Edades; }

        }

        public void GrabarEdad(Edad edad)
        {
            if (edad.EdadID == 0)
            {
                context.Edades.Add(edad);
            }
            else
            {
                Edad dbEntrada = context.Edades.Find(edad.EdadID);
                if (dbEntrada != null)
                {
                    dbEntrada.Desde = edad.Desde;
                    dbEntrada.Hasta = edad.Hasta;
                    dbEntrada.Rango = edad.Desde.ToString() + " - " + edad.Hasta.ToString();
                }
            }
            context.SaveChanges();
        }

        public Edad BorrarEdad(int id)
        {
            Edad EdadABorrar = context.Edades.Find(id);
            if (EdadABorrar != null)
            {
                context.Edades.Remove(EdadABorrar);
                context.SaveChanges();
            }
            return EdadABorrar;
        }
    }
}
