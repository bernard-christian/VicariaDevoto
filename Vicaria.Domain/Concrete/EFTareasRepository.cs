﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFTareasRepository : ITareasRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Tarea> Tareas
        {
            get { return context.Tareas; }

        }

        public void GrabarTarea(Tarea tarea)
        {
            if (tarea.TareaID == 0)
            {
                context.Tareas.Add(tarea);
            }
            else
            {
                Tarea dbEntrada = context.Tareas.Find(tarea.TareaID);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = tarea.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public Tarea BorrarTarea(int id)
        {
            Tarea tareaABorrar = context.Tareas.Find(id);
            if (tareaABorrar != null)
            {
                context.Tareas.Remove(tareaABorrar);
                context.SaveChanges();
            }
            return tareaABorrar;
        }
    }
}
