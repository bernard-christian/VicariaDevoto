﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFEstudiosRepository : IEstudiosRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<EstudioCursado> EstudiosCursados
        {
            get { return context.EstudiosCursados; }

        }

        public void GrabarEstudioCursado(EstudioCursado estudioCursado)
        {
            if (estudioCursado.EstudioCursadoID == 0)
            {
                context.EstudiosCursados.Add(estudioCursado);
            }
            else
            {
                EstudioCursado dbEntrada = context.EstudiosCursados.Find(estudioCursado.EstudioCursadoID);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = estudioCursado.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public EstudioCursado BorrarEstudioCursado(int id)
        {
            EstudioCursado estudioCursadoABorrar = context.EstudiosCursados.Find(id);
            if (estudioCursadoABorrar != null)
            {
                context.EstudiosCursados.Remove(estudioCursadoABorrar);
                context.SaveChanges();
            }
            return estudioCursadoABorrar;
        }
        
        
        
        
        
        
        
        
        
        
    }
}
