﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFJornadasLaboralesRepository : IJornadasLaboralesRepository
    {
         private EFDbContext context = new EFDbContext();

        public IEnumerable<JornadaLaboral> JornadasLaborales
        {
            get { return context.JornadasLaborales; }

        }

        public void GrabarJornadaLaboral(JornadaLaboral jornadaLaboral)
        {
            if (jornadaLaboral.JornadaLaboralID == 0)
            {
                context.JornadasLaborales.Add(jornadaLaboral);
            }
            else
            {
                JornadaLaboral dbEntrada = context.JornadasLaborales.Find(jornadaLaboral.JornadaLaboralID);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = jornadaLaboral.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public JornadaLaboral BorrarJornadaLaboral(int id)
        {
            JornadaLaboral jornadaLaboralABorrar = context.JornadasLaborales.Find(id);
            if (jornadaLaboralABorrar != null)
            {
                context.JornadasLaborales.Remove(jornadaLaboralABorrar);
                context.SaveChanges();
            }
            return jornadaLaboralABorrar;
        }

        public string GetJornada(int id)
        {
            JornadaLaboral  jornada= context.JornadasLaborales.Find(id);
            return jornada.Descripcion;

        }
    }
    
}
