﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFGrupoFamiliarRepository : IGrupoFamiliarRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<GrupoFamiliar> GruposFamiliares
        {
            get { return context.GruposFamiliares; }

        }

        public void GrabarGrupoFamiliar(GrupoFamiliar grupoFamiliar)
        {
            if (grupoFamiliar.GrupoFamiliarId == 0)
            {
                context.GruposFamiliares.Add(grupoFamiliar);
            }
            else
            {
                GrupoFamiliar dbEntrada = context.GruposFamiliares.Find(grupoFamiliar.GrupoFamiliarId);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = grupoFamiliar.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public GrupoFamiliar BorrarGrupoFamiliar(int id)
        {
            GrupoFamiliar grupoFamiliarABorrar = context.GruposFamiliares.Find(id);
            if (grupoFamiliarABorrar != null)
            {
                context.GruposFamiliares.Remove(grupoFamiliarABorrar);
                context.SaveChanges();
            }
            return grupoFamiliarABorrar;
        }
    }
}
