﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Vicaria.Domain.Concrete
{
    public class EFBusquedasRepository : IBusquedasRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Busqueda> Busquedas
        {
            get { return context.Busquedas; }

        }

        public IEnumerable<Categoria> Categorias
        {
            get { return context.Categorias; }

        }

        public IEnumerable<JornadaLaboral> JornadasLaborales
        {
            get { return context.JornadasLaborales; }

        }


        public void GrabarBusqueda(Busqueda busqueda)
        {
            if (busqueda.BusquedaID == 0)
            {
                context.Busquedas.Add(busqueda);
            }
            else
            {
                Busqueda dbEntrada = context.Busquedas.Find(busqueda.BusquedaID);
                if (dbEntrada != null)
                {
                    dbEntrada.CategoriaID = busqueda.CategoriaID;
                    dbEntrada.JornadaLaboralID = busqueda.JornadaLaboralID;
                    dbEntrada.BusquedaActiva = busqueda.BusquedaActiva;
                }
            }
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }

        public Busqueda BorrarBusqueda(int id)
        {
            Busqueda busquedaABorrar = context.Busquedas.Find(id);
            if (busquedaABorrar != null)
            {
                context.Busquedas.Remove(busquedaABorrar);
                context.SaveChanges();
            }
            return busquedaABorrar;
        }

        public Postulante getPostulante(int idPostulante)
        {
            Postulante postulante = context.Postulantes.Find(idPostulante);
            return postulante;
        }

        public Categoria getCategoria(int idCategoria)
        {
            Categoria categoria = context.Categorias.Find(idCategoria);
            return categoria;
        }

        public JornadaLaboral getJornadaLaboral(int idJornadaLaboral)
        {
            JornadaLaboral jornada = context.JornadasLaborales.Find(idJornadaLaboral);
            return jornada;
        }
    }
}
