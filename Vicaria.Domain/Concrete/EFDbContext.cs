﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Entities;
using System.Data.Entity;

namespace Vicaria.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public DbSet<Postulante> Postulantes { get; set; }
        public DbSet<EstadoCivil> EstadosCiviles { get; set; }
        public DbSet<EstudioCursado> EstudiosCursados { get; set; }
        public DbSet<Nacionalidad> Nacionalidades { get; set; }
        public DbSet<Provincia> Provincias { get; set; }
        public DbSet<TipoDeDocumento> TiposDeDocumentos { get; set; }
        public DbSet<GrupoFamiliar> GruposFamiliares { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<JornadaLaboral> JornadasLaborales { get; set; }
        public DbSet<Busqueda> Busquedas { get; set; }
        public DbSet<Empleador> Empleadores { get; set; }
        public DbSet<Tarea> Tareas { get; set; }
        public DbSet<Edad> Edades { get; set; }
        public DbSet<Oferta> Ofertas { get; set; }
        public DbSet<Entrevista> Entrevistas { get; set; }
        public DbSet<Contratacion> Contrataciones { get; set; }

        //esto es para que nome cambie el nombre de las tablas cuando arma los querys el EF
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EstadoCivil>().ToTable("EstadoCivil");
            modelBuilder.Entity<EstudioCursado>().ToTable("Estudiocursado");
            modelBuilder.Entity<Nacionalidad>().ToTable("Nacionalidad");
            modelBuilder.Entity<Postulante>().ToTable("Postulantes");
            modelBuilder.Entity<Provincia>().ToTable("Provincia");
            modelBuilder.Entity<TipoDeDocumento>().ToTable("TipoDeDocumento");
            modelBuilder.Entity<GrupoFamiliar>().ToTable("GrupoFamiliar");
            modelBuilder.Entity<Categoria>().ToTable("Categoria");
            modelBuilder.Entity<JornadaLaboral>().ToTable("JornadaLaboral");
            modelBuilder.Entity<Busqueda>().ToTable("Busqueda");
            modelBuilder.Entity<Empleador>().ToTable("Empleador");
            modelBuilder.Entity<Tarea>().ToTable("Tarea");
            modelBuilder.Entity<Edad>().ToTable("Edad");
            modelBuilder.Entity<Oferta>().ToTable("Oferta");
            modelBuilder.Entity<Entrevista>().ToTable("Entrevista");
            modelBuilder.Entity<Contratacion>().ToTable("Contratacion");

            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Entity<Oferta>()
            //    .HasMany(c => c.Tareas).WithMany(i => i.Ofertas)
            //    .Map(t => t.MapLeftKey("OfertaID")
            //        .MapRightKey("TareaID")
            //        .ToTable("OfertaTarea"));

        }
    }
}
