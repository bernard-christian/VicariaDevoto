﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFCategoriasRepository : ICategoriasRepository
    {
         private EFDbContext context = new EFDbContext();

        public IEnumerable<Categoria> Categorias
        {
            get { return context.Categorias; }

        }

        public void GrabarCategoria(Categoria categoria)
        {
            if (categoria.CategoriaID == 0)
            {
                context.Categorias.Add(categoria);
            }
            else
            {
                Categoria dbEntrada = context.Categorias.Find(categoria.CategoriaID);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = categoria.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public Categoria BorrarCategoria(int id)
        {
            Categoria categoriaABorrar = context.Categorias.Find(id);
            if (categoriaABorrar != null)
            {
                context.Categorias.Remove(categoriaABorrar);
                context.SaveChanges();
            }
            return categoriaABorrar;
        }

        public string GetCategoria(int id)
        {
            Categoria categoria = context.Categorias.Find(id);

            return categoria.Descripcion;
        }
    }
    
}
