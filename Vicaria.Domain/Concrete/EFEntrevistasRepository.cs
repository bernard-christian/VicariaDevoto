﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFEntrevistasRepository : IEntrevistaRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<Entrevista> Entrevistas
        {
            get { return context.Entrevistas; }

        }

        public void GrabarEntrevista(Entrevista entrevista)
        {
            if (entrevista.entrevistaID == 0)
            {
                context.Entrevistas.Add(entrevista);
            }
            else
            {
                Entrevista dbEntrada = context.Entrevistas.Find(entrevista.entrevistaID);
                if (dbEntrada != null)
                {
                    dbEntrada.OfertaID = entrevista.OfertaID;
                    dbEntrada.BusquedaID = entrevista.BusquedaID;
                    dbEntrada.fechaAlta = entrevista.fechaAlta;
                    dbEntrada.fechaEntrevista = entrevista.fechaEntrevista;
                    dbEntrada.Observaciones = entrevista.Observaciones;
                    dbEntrada.cubreVacante = entrevista.cubreVacante;

                }
            }
            context.SaveChanges();
        }

        public void GrabarOferta(Oferta oferta)
        {
            if (oferta.OfertaID == 0)
            {
                context.Ofertas.Add(oferta);
            }
            else
            {
                Oferta dbOferta = context.Ofertas.Find(oferta.OfertaID);
                if (dbOferta != null)
                {
                    
                    dbOferta.OfertaActiva = oferta.OfertaActiva;

                }
            }
            context.SaveChanges();
        }

        public void GrabarBusqueda(Busqueda busqueda)
        {
            if (busqueda.BusquedaID == 0)
            {
                context.Busquedas.Add(busqueda);
            }
            else
            {
                Busqueda dbEntrada = context.Busquedas.Find(busqueda.BusquedaID);
                if (dbEntrada != null)
                {
                    dbEntrada.CategoriaID = busqueda.CategoriaID;
                    dbEntrada.JornadaLaboralID = busqueda.JornadaLaboralID;
                    dbEntrada.BusquedaActiva = busqueda.BusquedaActiva;

                }
            }
            context.SaveChanges();
        }


        public Entrevista BorrarEntrevista(int entrevistaID)
        {
            Entrevista entrevistaABorrar = context.Entrevistas.Find(entrevistaID);
            if (entrevistaABorrar != null)
            {
                context.Entrevistas.Remove(entrevistaABorrar);
                context.SaveChanges();
            }
            return entrevistaABorrar;
        }

        public Oferta GetOferta(int id)
        {
            Oferta oferta = context.Ofertas.Find(id);
            return oferta;
        }

        public Busqueda GetBusqueda(int idBusqueda)
        {
            Busqueda busqueda = context.Busquedas.Find(idBusqueda);
            return busqueda;
        }
    }
}
