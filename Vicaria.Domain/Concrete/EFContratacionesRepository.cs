﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFContratacionesRepository : IContratacionesRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Contratacion> Contrataciones
        {
            get { return context.Contrataciones; }

        }

        public void GrabarContratacion(Contratacion contratacion)
        {
            if (contratacion.ContratacionID == 0)
            {
                context.Contrataciones.Add(contratacion);
            }
            else
            {
                Contratacion dbEntrada = context.Contrataciones.Find(contratacion.ContratacionID);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = contratacion.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public Contratacion BorrarContratacion(int id)
        {
            Contratacion contratacionABorrar = context.Contrataciones.Find(id);
            if (contratacionABorrar != null)
            {
                context.Contrataciones.Remove(contratacionABorrar);
                context.SaveChanges();
            }
            return contratacionABorrar;
        }
    }
}
