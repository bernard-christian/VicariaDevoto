﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFNacionalidadesRepository : INacionalidadesRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Nacionalidad> Nacionalidades
        {
            get { return context.Nacionalidades; }

        }

        public void GrabarNacionalidad(Nacionalidad nacionalidad)
        {
            if (nacionalidad.NacionalidadID == 0)
            {
                context.Nacionalidades.Add(nacionalidad);
            }
            else
            {
                Nacionalidad dbEntrada = context.Nacionalidades.Find(nacionalidad.NacionalidadID);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = nacionalidad.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public Nacionalidad BorrarNacionalidad(int id)
        {
            Nacionalidad nacionalidadABorrar = context.Nacionalidades.Find(id);
            if (nacionalidadABorrar != null)
            {
                context.Nacionalidades.Remove(nacionalidadABorrar);
                context.SaveChanges();
            }
            return nacionalidadABorrar;
        }
    }
}
