﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.Domain.Concrete
{
    public class EFDocumentosRepository : IDocumentosRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<TipoDeDocumento> TiposDeDocumentos
        {
            get { return context.TiposDeDocumentos; }

        }

        public void GrabarDocumento(TipoDeDocumento tipoDeDocumento)
        {
            if (tipoDeDocumento.TipoDeDocumentoID == 0)
            {
                context.TiposDeDocumentos.Add(tipoDeDocumento);
            }
            else
            {
                TipoDeDocumento dbEntrada = context.TiposDeDocumentos.Find(tipoDeDocumento.TipoDeDocumentoID);
                if (dbEntrada != null)
                {
                    dbEntrada.Descripcion = tipoDeDocumento.Descripcion;
                }
            }
            context.SaveChanges();
        }

        public TipoDeDocumento BorrarDocumento(int id)
        {
            TipoDeDocumento documentoABorrar = context.TiposDeDocumentos.Find(id);
            if (documentoABorrar != null)
            {
                context.TiposDeDocumentos.Remove(documentoABorrar);
                context.SaveChanges();
            }
            return documentoABorrar;
        }
    }
}
