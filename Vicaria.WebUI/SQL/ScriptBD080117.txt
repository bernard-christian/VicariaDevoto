﻿USE Vicaria;  
GO 

ALTER TABLE Entrevista ADD Postulantes_Postulante_ID int NULL;  


update
	Entrevista
set
	Entrevista.Postulantes_Postulante_ID = Bu.Postulante_PostulanteID
from
	Entrevista as En
inner join 
	Busqueda as Bu
on 
	Bu.BusquedaID = Busqueda_BusquedaID

ALTER TABLE Entrevista ALTER COLUMN Postulantes_Postulante_ID INTEGER NOT NULL


--rango de edad de postulantes
ALTER TABLE Oferta ADD EdadMin int NULL;
ALTER TABLE Oferta ADD EdadMax int NULL;

update
	Oferta
set
	Oferta.EdadMin = e.Desde,
	Oferta.EdadMax = e.Hasta
from
	Oferta as o
inner join 
	Edad as e
on e.EdadID = Edad_EdadID

ALTER TABLE Oferta ADD EdadMin int NOY NULL;
ALTER TABLE Oferta ADD EdadMax int NULL;

ALTER TABLE Oferta ALTER COLUMN EdadMin INTEGER NOT NULL
ALTER TABLE Oferta ALTER COLUMN EdadMax INTEGER NOT NULL

ALTER TABLE Oferta DROP COLUMN Edad_EdadID ; 

--agrego la tabla de contratacion
CREATE TABLE Contratacion (
    ContratacionID int NOT NULL
             IDENTITY(1, 1),
    Descripcion nvarchar(50) NOT NULL
);

--agrego la columna de contataciones y tarea en ofertas
ALTER TABLE Oferta ADD Contratacion_ContratacionID int NULL;
ALTER TABLE Oferta ADD Tarea nvarchar(200) NULL;

--cambio el nombre de la tabla de relacion de oferta con tarea
exec sp_rename 'dbo.OfertaTarea', 'oldOfertaTarea'

-reemplazo en el nuevo campo tarea las tareas que ya existian en el viejo formato
BEGIN

DECLARE @rowCount INT;
DECLARE @currentRow INT;
DECLARE @descTarea VARCHAR(50);
DECLARE @idOferta INT;
SET @rowCount = (SELECT COUNT(*) FROM oldOfertaTarea);
SET @currentRow = 1;

update Oferta set tarea = ' '
select * into #temp from (
select ot.OfertaID as OfertaId, ta.Descripcion as Descripcion,  Row_Number() Over ( Order By ot.OfertaID ) As Num from oldOfertaTarea as ot 
inner join Tarea as ta on ot.TareaId = ta.TareaID 
) as data


WHILE (@currentRow < @rowCount) 
	BEGIN
	
			SET @descTarea = (SELECT Descripcion FROM #temp WHERE Num = @currentRow);
			SET @descTarea =  REPLACE(@descTarea, ' ', '')	
			SET @idOferta = (SELECT OfertaID FROM #temp WHERE Num = @currentRow);
			UPDATE Oferta SET Tarea =  Tarea + ' ' + @descTarea where OfertaId = @idOferta ;
			
			SET @currentRow = @currentRow + 1;
		
	END 
drop table #temp
END


update Postulantes
set EstudiosTerminados = 0