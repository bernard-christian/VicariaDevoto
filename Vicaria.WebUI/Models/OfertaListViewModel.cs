﻿using System;
using System.Collections.Generic;
using Vicaria.Domain.Entities;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vicaria.WebUI.Models
{
    public class OfertasListViewModel
    {
        public List<Oferta> Ofertas { get; set; }
        public  int idActual { get; set; }
        public string nombreActual { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}