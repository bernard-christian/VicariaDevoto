﻿using System;
using System.Collections.Generic;
using Vicaria.Domain.Entities;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vicaria.WebUI.Models
{
    public class TareasDeOfertaListViewModel
    {
        public int TareaID { get; set; }
        public string Descripcion { get; set; }
        public bool Asignada { get; set; }
    }
}