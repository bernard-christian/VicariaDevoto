﻿using System;
using System.Collections.Generic;
using Vicaria.Domain.Entities;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vicaria.WebUI.Models
{
    public class EnlacesPosiblesListViewModel
    {
        //public IEnumerable<Postulante> postulantesPosibles { get; set; }
        public IEnumerable<Busqueda> busquedasPosibles { get; set; }
        public  Oferta oferta { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}