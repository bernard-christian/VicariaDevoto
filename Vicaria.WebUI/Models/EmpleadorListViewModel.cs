﻿using System;
using System.Collections.Generic;
using Vicaria.Domain.Entities;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vicaria.WebUI.Models
{
    public class EmpleadorListViewModel
    {
        public IEnumerable<Empleador> Empleadores { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}