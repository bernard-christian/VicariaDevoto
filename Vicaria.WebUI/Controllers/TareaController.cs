﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class TareaController : Controller
    {
       private ITareasRepository repository;

       public TareaController(ITareasRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarTareas()
        {
            return View(repository.Tareas);
        }

        public ViewResult EditarTarea(int Id)
        {
            Tarea  tarea = repository.Tareas.FirstOrDefault(x => x.TareaID == Id);
            return View(tarea);
        }

        [HttpPost]
        public ActionResult EditarTarea(Tarea tarea)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarTarea(tarea);
                TempData["message"] = string.Format("{0} fué grabada corretamente", tarea.Descripcion);
                return RedirectToAction("ListarTareas");
            }
            else
            {
                //alg salio malo en os datos
                return View(tarea);
            }
        }

        public ViewResult CrearTarea()
        {
            return View("EditarTarea", new Tarea());
        }

        public ViewResult BorrarTarea(int Id)
        {
            Tarea tarea = repository.Tareas.FirstOrDefault(p => p.TareaID == Id);
            return View(tarea);
        }

        [HttpPost]
        public ActionResult BorrarTarea(Tarea tarea)
        {
            Tarea tareaBorrada = repository.BorrarTarea(tarea.TareaID);
            if (tareaBorrada != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", tareaBorrada.Descripcion);
            }
            return RedirectToAction("ListarTareas");
        }
    }
}