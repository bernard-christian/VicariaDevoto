﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class NacionalidadController : Controller
    {
       private INacionalidadesRepository repository;

       public NacionalidadController(INacionalidadesRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarNacionalidades()
        {
            return View(repository.Nacionalidades);
        }

        public ViewResult EditarNacionalidad(int Id)
        {
            Nacionalidad nacionalidad = repository.Nacionalidades.FirstOrDefault(x => x.NacionalidadID == Id);
            return View(nacionalidad);
        }

        [HttpPost]
        public ActionResult EditarNacionalidad(Nacionalidad nacionalidad)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarNacionalidad(nacionalidad);
                TempData["message"] = string.Format("{0} fué grabado corretamente", nacionalidad.Descripcion);
                return RedirectToAction("ListarNacionalidades");
            }
            else
            {
                //alg salio malo en os datos
                return View(nacionalidad);
            }
        }

        public ViewResult CrearNacionalidad()
        {
            return View("EditarNacionalidad", new Nacionalidad());
        }

        public ViewResult BorrarNacionalidad(int Id)
        {
            Nacionalidad nacionalidad = repository.Nacionalidades.FirstOrDefault(p => p.NacionalidadID == Id);
            return View(nacionalidad);
        }

        [HttpPost]
        public ActionResult BorrarNacionalidad(Nacionalidad nacionalidad)
        {
            Nacionalidad nacionalidadBorrada = repository.BorrarNacionalidad(nacionalidad.NacionalidadID);
            if (nacionalidadBorrada != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", nacionalidadBorrada.Descripcion);
            }
            return RedirectToAction("ListarNacionalidades");
        }
    }
}