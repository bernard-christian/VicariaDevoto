﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System.Diagnostics;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using Vicaria.WebUI.Models;
using System.Data.Entity;

namespace Vicaria.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IOfertasRepository repository;
        private IEntrevistaRepository repoEntrevista;
        
        public int PageSize = 100; //tamaño de página de la grilla de postulantes
       


        public HomeController(IOfertasRepository repo, IEntrevistaRepository repo2)
        {
            repository = repo;
            repoEntrevista = repo2;
        }

        public ActionResult Index(int page = 1)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fileVersionInfo.ProductVersion;
            ViewBag.version = version;
            ViewBag.Jornadas = new SelectList(repository.JornadasLaborales, "JornadaLaboralID", "Descripcion");
            ViewBag.Categorias = new SelectList(repository.Categorias, "CategoriaId", "Descripcion");
            return View();

        }

        public ActionResult Informacion()
        {
            return View();
        }
    }
}