﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class EstudioController : Controller
    {
        private IEstudiosRepository repository;

        public EstudioController(IEstudiosRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarEstudiosCursados()
        {
            return View(repository.EstudiosCursados);
        }

        public ViewResult EditarEstudioCursado(int Id)
        {
            EstudioCursado estudioCursado = repository.EstudiosCursados.FirstOrDefault(x => x.EstudioCursadoID == Id);
            return View(estudioCursado);
        }

        [HttpPost]
        public ActionResult EditarEstudioCursado(EstudioCursado estudioCursado)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarEstudioCursado(estudioCursado);
                TempData["message"] = string.Format("{0} fué grabado corretamente", estudioCursado.Descripcion);
                return RedirectToAction("ListarEstudiosCursados");
            }
            else
            {
                //alg salio malo en os datos
                return View(estudioCursado);
            }
        }

        public ViewResult CrearEstudioCursado()
        {
            return View("EditarEstudioCursado", new EstudioCursado());
        }

        public ViewResult BorrarEstudioCursado(int Id)
        {
            EstudioCursado estudioCursado = repository.EstudiosCursados.FirstOrDefault(p => p.EstudioCursadoID == Id);
            return View(estudioCursado);
        }

        [HttpPost]
        public ActionResult BorrarEstudioCursado(EstudioCursado estudioCursado)
        {
            EstudioCursado estudioCursadoBorrado = repository.BorrarEstudioCursado(estudioCursado.EstudioCursadoID);
            if (estudioCursadoBorrado != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", estudioCursadoBorrado.Descripcion);
            }
            return RedirectToAction("ListarEstudiosCursados");
        }
    }
}