﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using Vicaria.WebUI.Models;
using System.Collections.Specialized;
using System.Data.Entity.Validation;

namespace Vicaria.WebUI.Controllers
{
    public class EmpleadorController : Controller
    {
        private IEmpleadorRepository repository;
        public int PageSize = 100; //tamaño de página de la grilla de postulantes

        public EmpleadorController(IEmpleadorRepository empleadorRepository)
        {
            this.repository = empleadorRepository;
        }

        public ViewResult Borrar(int Id)
        {
            Empleador empleador = repository.Empleadores.FirstOrDefault(p => p.EmpleadorID == Id);
            return View(empleador);
        }

        [HttpPost]
        public ActionResult Borrar(Empleador empleador)
        {
            Empleador empleadorBorrado = repository.BorrarEmpleador(empleador.EmpleadorID);
            if (empleadorBorrado != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", empleadorBorrado.Apellido);
            }
            return RedirectToAction("List");
        }

        public ViewResult Crear()
        {
            ViewBag.Documentos = new SelectList(repository.TiposDeDocumentos, "TipoDeDocumentoID", "Descripcion");
            ViewBag.Provincias = new SelectList(repository.Provincias, "ProvinciaID", "Nombre");
            Empleador empleador = new Empleador();
            empleador.FechaAlta = DateTime.Now;
            empleador.keepAlive = DateTime.Now;
            return View("Editar", empleador);
        }

        public ViewResult List(int page = 1)
        {
            EmpleadorListViewModel model = new EmpleadorListViewModel
            {
                Empleadores = repository.Empleadores
                    .OrderByDescending(p => p.keepAlive)
                    .Skip((page-1) * PageSize)
                    .Take(PageSize),
                    PagingInfo = new PagingInfo{
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems = repository.Empleadores.Count()
                    }
            };
            return View(model);
        }

        public ViewResult Buscar( string busqueda, int page = 1) 
        {
            EmpleadorListViewModel model = new EmpleadorListViewModel();

            int dni = 0;
            bool esDni = int.TryParse(busqueda, out dni);
            
           
                if (esDni )
                {
                    model = new EmpleadorListViewModel
                    {
                        Empleadores = repository.Empleadores
                            .Where(e => e.Documento == dni)
                            .OrderByDescending(p => p.keepAlive)
                            .Skip((page - 1) * PageSize)
                            .Take(PageSize),
                        PagingInfo = new PagingInfo
                        {
                            CurrentPage = page,
                            ItemsPerPage = PageSize,
                            TotalItems = repository.Empleadores.Where(e => e.Documento == dni).Count()
                        }
                    };

                }
                else
                {
                    model = new EmpleadorListViewModel
                    {
                        Empleadores = repository.Empleadores
                            .Where(e => e.Apellido.ToUpper().Contains(busqueda.ToUpper()) || e.Nombre.ToUpper().Contains(busqueda.ToUpper()))
                            .OrderByDescending(p => p.keepAlive)
                            .Skip((page - 1) * PageSize)
                            .Take(PageSize),
                        PagingInfo = new PagingInfo
                        {
                            CurrentPage = page,
                            ItemsPerPage = PageSize,
                            TotalItems = repository.Empleadores.Where(e => e.Apellido.Contains(busqueda)).Count()
                        }
                    };


                };
             return View("List",model);



            





            }
           
       

       

        public ViewResult Editar(int Id)
        {
            ViewBag.Documentos = new SelectList(repository.TiposDeDocumentos, "TipoDeDocumentoID", "Descripcion");
            ViewBag.Provincias = new SelectList(repository.Provincias, "ProvinciaID", "Nombre");
            Empleador empleador = repository.Empleadores.FirstOrDefault(p => p.EmpleadorID == Id);
            return View(empleador);
        }
        
        [HttpPost]
        public ActionResult Editar(Empleador empleador)
        {
            ViewBag.Documentos = new SelectList(repository.TiposDeDocumentos, "TipoDeDocumentoID", "Descripcion");
            ViewBag.Provincias = new SelectList(repository.Provincias, "ProvinciaID", "Nombre");
            if (ModelState.IsValid)
            {
                repository.GrabarEmpleador(empleador);
                TempData["message"] = string.Format("{0} fué grabado corretamente", empleador.Apellido);
                return RedirectToAction("List");
            }
            else
            {
                //algo salio malo en os datos
                return View(empleador);
            }
        }

        public ViewResult Detallar(int Id, int retorno = 0)
        {

            Empleador empleador = repository.Empleadores.FirstOrDefault(p => p.EmpleadorID == Id);

            ViewBag.Documentos = new SelectList(repository.TiposDeDocumentos, "ID", "Descripcion");
            ViewBag.Provincias = new SelectList(repository.Provincias, "ProvinciaID", "Nombre");
            ViewBag.Retorno = retorno;

            return View(empleador);
        }

        public ActionResult KeepAlive(int Id)
        {
            Empleador empleador = repository.Empleadores.FirstOrDefault(p => p.EmpleadorID == Id);
            empleador.keepAlive = DateTime.Now;
            if (ModelState.IsValid)
            {
                repository.GrabarEmpleador(empleador);
                TempData["message"] = string.Format("{0} fué refrescado corretamente", empleador.Apellido);
                return RedirectToAction("List");
            }
            else
            {
                //algo salio malo en os datos
                return View(empleador);
            }
        }
    }
}