﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class DocumentoController : Controller
    {
        private IDocumentosRepository repository;

        public DocumentoController(IDocumentosRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarDocumentos()
        {
            return View(repository.TiposDeDocumentos);
        }

        public ViewResult EditarDocumento(int Id)
        {
            TipoDeDocumento documento = repository.TiposDeDocumentos.FirstOrDefault(x => x.TipoDeDocumentoID == Id);
            return View(documento);
        }

        [HttpPost]
        public ActionResult EditarDocumento(TipoDeDocumento tipoDeDocumento)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarDocumento(tipoDeDocumento);
                TempData["message"] = string.Format("{0} fué grabado corretamente", tipoDeDocumento.Descripcion);
                return RedirectToAction("ListarDocumentos");
            }
            else
            {
                //alg salio malo en os datos
                return View(tipoDeDocumento);
            }
        }

        public ViewResult CrearDocumento()
        {
            return View("EditarDocumento", new TipoDeDocumento());
        }

        public ViewResult BorrarDocumento(int Id)
        {
            TipoDeDocumento tipoDeDocumento = repository.TiposDeDocumentos.FirstOrDefault(p => p.TipoDeDocumentoID == Id);
            return View(tipoDeDocumento);
        }

        [HttpPost]
        public ActionResult BorrarDocumento(TipoDeDocumento tipoDeDocumento)
        {
            TipoDeDocumento documentoBorrado = repository.BorrarDocumento(tipoDeDocumento.TipoDeDocumentoID);
            if (documentoBorrado != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", documentoBorrado.Descripcion);
            }
            return RedirectToAction("ListarDocumentos");
        }
    }
}