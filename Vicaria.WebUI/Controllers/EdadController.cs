﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class EdadController : Controller
    {
       private IEdadRepository repository;

       public EdadController(IEdadRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarEdades()
        {
            return View(repository.Edades);
        }

        public ViewResult EditarEdad(int Id)
        {
            Edad edad = repository.Edades.FirstOrDefault(x => x.EdadID == Id);
            return View(edad);
        }

        [HttpPost]
        public ActionResult EditarEdad(Edad edad)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarEdad(edad);
                return RedirectToAction("ListarEdades");
            }
            else
            {
                //alg salio malo en os datos
                return View(edad);
            }
        }

        public ViewResult CrearEdad()
        {
            return View("EditarEdad", new Edad ());
        }

        public ViewResult BorrarEdad(int Id)
        {
            Edad edad = repository.Edades.FirstOrDefault(p => p.EdadID == Id);
            return View(edad);
        }

        [HttpPost]
        public ActionResult BorrarEdad(Edad edad)
        {
            Edad edadBorrada = repository.BorrarEdad(edad.EdadID);
            if (edadBorrada != null)
            {
                TempData["message"] = string.Format("El rango desde {0} hasta {1} fué borrado", edadBorrada.Desde, edadBorrada.Hasta);
            }
            return RedirectToAction("ListarEdades");
        }
    }
}