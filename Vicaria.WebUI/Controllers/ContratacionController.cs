﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class ContratacionController : Controller
    {
        private IContratacionesRepository repository;

        public ContratacionController(IContratacionesRepository repo)
        {
            repository = repo;
    }
        
        public ActionResult ListarContrataciones()
        {
            return View(repository.Contrataciones);
        }

        public ViewResult EditarContratacion(int Id)
        {
            Contratacion contratacion = repository.Contrataciones.FirstOrDefault(ec => ec.ContratacionID == Id);
            return View(contratacion);
        }

        [HttpPost]
        public ActionResult EditarContratacion(Contratacion contratacion)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarContratacion(contratacion);
                TempData["message"] = string.Format("{0} fué grabado corretamente", contratacion.Descripcion);
                return RedirectToAction("ListarContrataciones");
            }
            else
            {
                //alg salio malo en os datos
                return View(contratacion);
            }
        }

        public ViewResult CrearContratacion()
        {
            return View("EditarContratacion", new Contratacion());
        }

        public ViewResult BorrarContratacion(int Id)
        {
            Contratacion contratacion = repository.Contrataciones.FirstOrDefault(p => p.ContratacionID == Id);
            return View(contratacion);
        }

        [HttpPost]
        public ActionResult BorrarContratacion(Contratacion contratacion)
        {
            Contratacion contratacionBorrada = repository.BorrarContratacion(contratacion.ContratacionID);
            if (contratacionBorrada != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", contratacionBorrada.Descripcion);
            }
            return RedirectToAction("ListarContrataciones");
        }
    }
}