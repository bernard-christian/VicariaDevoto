﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class ProvinciaController : Controller
    {
        private IProvinciasRepository repository;

        public ProvinciaController(IProvinciasRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarProvincias()
        {
            return View(repository.Provincias);
        }

        public ViewResult EditarProvincia(int Id)
        {
            Provincia provincia = repository.Provincias.FirstOrDefault(x => x.ProvinciaId == Id);
            return View(provincia);
        }

        [HttpPost]
        public ActionResult EditarProvincia(Provincia provincia)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarProvincia(provincia);
                TempData["message"] = string.Format("{0} fué grabado corretamente", provincia.Nombre);
                return RedirectToAction("ListarProvincias");
            }
            else
            {
                //alg salio malo en os datos
                return View(provincia);
            }
        }

        public ViewResult CrearProvincia()
        {
            return View("EditarProvincia", new Provincia());
        }

        public ViewResult BorrarProvincia(int Id)
        {
            Provincia provincia = repository.Provincias.FirstOrDefault(p => p.ProvinciaId == Id);
            return View(provincia);
        }

        [HttpPost]
        public ActionResult BorrarProvincia(Provincia provincia)
        {
            Provincia provinciaBorrada = repository.BorrarProvincia(provincia.ProvinciaId);
            if (provinciaBorrada != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", provinciaBorrada.Nombre);
            }
            return RedirectToAction("ListarProvincias");
        }
    }
}