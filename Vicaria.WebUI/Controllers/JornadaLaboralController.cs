﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class JornadaLaboralController : Controller
    {
         private IJornadasLaboralesRepository repository;

        public JornadaLaboralController(IJornadasLaboralesRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarJornadasLaborales()
        {
            return View(repository.JornadasLaborales);
        }

        public ViewResult EditarJornadaLaboral(int Id)
        {
            JornadaLaboral jornadaLaboral = repository.JornadasLaborales.FirstOrDefault(x => x.JornadaLaboralID == Id);
            return View(jornadaLaboral);
        }

        [HttpPost]
        public ActionResult EditarJornadaLaboral(JornadaLaboral jornadaLaboral)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarJornadaLaboral(jornadaLaboral);
                TempData["message"] = string.Format("{0} fué grabado corretamente", jornadaLaboral.Descripcion);
                return RedirectToAction("ListarJornadasLaborales");
            }
            else
            {
                //alg salio malo en os datos
                return View(jornadaLaboral);
            }
        }

        public ViewResult CrearJornadaLaboral()
        {
            return View("EditarJornadaLaboral", new JornadaLaboral());
        }

        public ViewResult BorrarJornadaLaboral(int Id)
        {
            JornadaLaboral jornadaLaboral = repository.JornadasLaborales.FirstOrDefault(p => p.JornadaLaboralID == Id);
            return View(jornadaLaboral);
        }

        [HttpPost]
        public ActionResult BorrarJornadaLaboral(JornadaLaboral jornadaLaboral)
        {
            JornadaLaboral jornadaLaboralBorrada = repository.BorrarJornadaLaboral(jornadaLaboral.JornadaLaboralID);
            if (jornadaLaboralBorrada != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", jornadaLaboralBorrada.Descripcion);
            }
            return RedirectToAction("ListarJornadasLaborales");
        }
    }
}