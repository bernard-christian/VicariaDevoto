﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class EstadoCivilController : Controller
    {
        private IAdminRepository repository;

        public EstadoCivilController(IAdminRepository repo)
        {
            repository = repo;
        }

        public ActionResult Index()
        {
            return View(repository.EstadosCiviles);
        }

        public ViewResult EditarEstadoCivil(int Id)
        {
            EstadoCivil estadoCivil = repository.EstadosCiviles.FirstOrDefault(ec => ec.EstadoCivilID == Id);
            return View(estadoCivil);
        }

        [HttpPost]
        public ActionResult EditarEstadoCivil(EstadoCivil estadocivil)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarEstadoCivil(estadocivil);
                TempData["message"] = string.Format("{0} fué grabado corretamente", estadocivil.Descripcion);
                return RedirectToAction("Index");
            }
            else
            {
                //alg salio malo en os datos
                return View(estadocivil);
            }
        }

        public ViewResult CrearEstadoCivil()
        {
            return View("EditarEstadoCivil", new EstadoCivil());
        }

        public ViewResult BorrarEstadoCivil(int Id)
        {
            EstadoCivil estadoCivil = repository.EstadosCiviles.FirstOrDefault(p => p.EstadoCivilID == Id);
            return View(estadoCivil);
        }

        [HttpPost]
        public ActionResult BorrarEstadoCivil(EstadoCivil estadoCivil)
        {
            EstadoCivil estadoCivilBorrado = repository.BorrarEstadoCivil(estadoCivil.EstadoCivilID);
            if (estadoCivilBorrado != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", estadoCivilBorrado.Descripcion);
            }
            return RedirectToAction("Index");
        }
    }
}
