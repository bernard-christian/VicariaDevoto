﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class CategoriaController : Controller
    {
        private ICategoriasRepository repository;

        public CategoriaController(ICategoriasRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarCategorias()
        {
            return View(repository.Categorias);
        }

        public ViewResult EditarCategoria(int Id)
        {
            Categoria categoria = repository.Categorias.FirstOrDefault(x => x.CategoriaID == Id);
            return View(categoria);
        }

        [HttpPost]
        public ActionResult EditarCategoria(Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarCategoria(categoria);
                TempData["message"] = string.Format("{0} fué grabado corretamente", categoria.Descripcion);
                return RedirectToAction("ListarCategorias");
            }
            else
            {
                //alg salio malo en os datos
                return View(categoria);
            }
        }

        public ViewResult CrearCategoria()
        {
            return View("EditarCategoria", new Categoria());
        }

        public ViewResult BorrarCategoria(int Id)
        {
            Categoria categoria = repository.Categorias.FirstOrDefault(p => p.CategoriaID == Id);
            return View(categoria);
        }

        [HttpPost]
        public ActionResult BorrarCategoria(Categoria categoria)
        {
            Categoria categoriaBorrada = repository.BorrarCategoria(categoria.CategoriaID);
            if (categoriaBorrada != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", categoriaBorrada.Descripcion);
            }
            return RedirectToAction("ListarCategorias");
        }
    }
}