﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;

namespace Vicaria.WebUI.Controllers
{
    public class GrupoFamiliarController : Controller
    {
        private IGrupoFamiliarRepository repository;

        public GrupoFamiliarController(IGrupoFamiliarRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListarGruposFamiliares()
        {
            return View(repository.GruposFamiliares);
        }

        public ViewResult EditarGrupoFamiliar(int Id)
        {
            GrupoFamiliar grupoFamiliar = repository.GruposFamiliares.FirstOrDefault(x => x.GrupoFamiliarId == Id);
            return View(grupoFamiliar);
        }

        [HttpPost]
        public ActionResult EditarGrupoFamiliar(GrupoFamiliar grupoFamiliar)
        {
            if (ModelState.IsValid)
            {
                repository.GrabarGrupoFamiliar(grupoFamiliar);
                TempData["message"] = string.Format("{0} fué grabado corretamente", grupoFamiliar.Descripcion);
                return RedirectToAction("ListarGruposFamiliares");
            }
            else
            {
                //alg salio malo en os datos
                return View(grupoFamiliar);
            }
        }

        public ViewResult CrearGrupoFamiliar()
        {
            return View("EditarGrupoFamiliar", new GrupoFamiliar());
        }

        public ViewResult BorrarGrupoFamiliar(int Id)
        {
            GrupoFamiliar grupoFamiliar = repository.GruposFamiliares.FirstOrDefault(p => p.GrupoFamiliarId == Id);
            return View(grupoFamiliar);
        }

        [HttpPost]
        public ActionResult BorrarGrupoFamiliar(GrupoFamiliar grupoFamiliar)
        {
            GrupoFamiliar grupoFamiliarBorrado = repository.BorrarGrupoFamiliar(grupoFamiliar.GrupoFamiliarId);
            if (grupoFamiliarBorrado != null)
            {
                TempData["message"] = string.Format("{0} fué borrado", grupoFamiliarBorrado.Descripcion);
            }
            return RedirectToAction("ListarGruposFamiliares");
        }
    }
}