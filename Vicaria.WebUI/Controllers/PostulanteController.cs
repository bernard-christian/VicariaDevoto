﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using Vicaria.WebUI.Models;
using System.Collections.Specialized;

namespace Vicaria.WebUI.Controllers
{
    public class PostulanteController : Controller
    {
        private IPostulanteRepository repository;
        public int PageSize = 100; //tamaño de página de la grilla de postulantes

        public PostulanteController(IPostulanteRepository postulanteRepository)
        {
            this.repository = postulanteRepository;
        }

        public ViewResult Borrar(int Id)
        {
            Postulante postulante = repository.Postulantes.FirstOrDefault(p => p.PostulanteID == Id);
            return View(postulante);
        }

        [HttpPost]
        public ActionResult Borrar(Postulante postulante)
        {
            Postulante postulannteBorrado = repository.BorrarPostulante(postulante.PostulanteID);
            if (postulannteBorrado != null)
            {
                TempData["message"]=string.Format("{0} fué borrado", postulannteBorrado.Apellido);
            }
            return RedirectToAction("List");
        }

        public ActionResult KeepAlive(int Id)
        {
            Postulante postulante = repository.Postulantes.FirstOrDefault(p => p.PostulanteID == Id);
            postulante.KeepAlive = DateTime.Now;
            if (ModelState.IsValid)
            {
                repository.GrabarPostulante(postulante);
                TempData["message"] = string.Format("{0} fué refrescado corretamente", postulante.Apellido);
                return RedirectToAction("List");
            }
            else
            {
                //algo salio malo en os datos
                return View(postulante);
            }
        }

        public ViewResult Crear()
        {
            ViewBag.Lista = new SelectList(repository.EstadosCiviles, "EstadoCivilId", "Descripcion");
            ViewBag.Documentos = new SelectList(repository.TiposDeDocumentos, "TipoDeDocumentoID", "Descripcion");
            ViewBag.Nacionalidades = new SelectList(repository.Nacionalidades, "NacionalidadId", "Descripcion");
            ViewBag.Estudios = new SelectList(repository.Estudios, "EstudioCursadoID", "Descripcion");
            ViewBag.Provincias = new SelectList(repository.Provincias, "ProvinciaID", "Nombre");
            ViewBag.GruposFamiliares = new SelectList(repository.GruposFamiliares, "GrupoFamiliarID", "Descripcion");
            Postulante postulante = new Postulante();
            postulante.FechaDeNacimiento = DateTime.Now;
            postulante.KeepAlive = DateTime.Now;
            postulante.FechaAlta = DateTime.Now;
            return View("Editar", postulante);
        }

        public ViewResult List(int page = 1)
        {
            PostulanteListViewModel model = new PostulanteListViewModel
            {
                Postulantes = repository.Postulantes
                    .OrderByDescending(p => p.KeepAlive)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Postulantes.Count()
                }

            };
            return View(model);
        }




        public ViewResult Buscar(string Busqueda, int page = 1)
        {
            PostulanteListViewModel model = new PostulanteListViewModel();

            int dni = 0;
            bool esDni = int.TryParse(Busqueda, out dni);
            if (esDni)
            {
                model = new PostulanteListViewModel
                {
                    Postulantes = repository.Postulantes
                        .Where(p => p.Documento == dni)
                        .OrderByDescending(p => p.KeepAlive)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems = repository.Postulantes.Where(p => p.Documento == dni).Count()
                    }
                };
            }
            else
            {
                 model = new PostulanteListViewModel
                {
                    Postulantes = repository.Postulantes
                        .Where(e => e.Apellido.ToUpper().Contains(Busqueda.ToUpper()) || e.Nombre.ToUpper().Contains(Busqueda.ToUpper()))
                        .OrderByDescending(p => p.KeepAlive)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems = repository.Postulantes.Where(p => p.Apellido.Contains(Busqueda)).Count()
                    }
                };
            }

            return View("List", model);
        }

        public ViewResult Editar(int Id)
        {
            ViewBag.Lista = new SelectList(repository.EstadosCiviles, "EstadoCivilId", "Descripcion");
            ViewBag.Documentos = new SelectList(repository.TiposDeDocumentos, "TipoDeDocumentoID", "Descripcion");
            ViewBag.Nacionalidades = new SelectList(repository.Nacionalidades, "NacionalidadID", "Descripcion");
            ViewBag.Estudios = new SelectList(repository.Estudios, "EstudioCursadoId", "Descripcion");
            ViewBag.Provincias = new SelectList(repository.Provincias, "ProvinciaID", "Nombre");
            ViewBag.GruposFamiliares = new SelectList(repository.GruposFamiliares, "GrupoFamiliarID", "Descripcion");
            Postulante postulante = repository.Postulantes.FirstOrDefault(p => p.PostulanteID == Id);
            return View(postulante);
        }
        
        [HttpPost]
        public ActionResult Editar(Postulante postulante)
        {
            ViewBag.Lista = new SelectList(repository.EstadosCiviles, "EstadoCivilId", "Descripcion");
            ViewBag.Documentos = new SelectList(repository.TiposDeDocumentos, "TipoDeDocumentoID", "Descripcion");
            ViewBag.Nacionalidades = new SelectList(repository.Nacionalidades, "NacionalidadID", "Descripcion");
            ViewBag.Estudios = new SelectList(repository.Estudios, "EstudioCursadoId", "Descripcion");
            ViewBag.Provincias = new SelectList(repository.Provincias, "ProvinciaID", "Nombre");
            ViewBag.GruposFamiliares = new SelectList(repository.GruposFamiliares, "GrupoFamiliarID", "Descripcion");
            if (ModelState.IsValid)
            {
                repository.GrabarPostulante(postulante);
                TempData["message"] = string.Format("{0} fué grabado corretamente", postulante.Apellido);
                return RedirectToAction("List");
            }
            else
            {
                //algo salio malo en os datos
                return View(postulante);
            }
        }

        public ViewResult Detallar(int Id, int retorno = 0){
        
            Postulante postulante = repository.Postulantes.FirstOrDefault(p => p.PostulanteID == Id);

            ViewBag.Lista = new SelectList(repository.EstadosCiviles, "EstadoCivilId", "Descripcion");
            ViewBag.Documentos = new SelectList(repository.TiposDeDocumentos, "ID", "Descripcion");
            ViewBag.Nacionalidades = new SelectList(repository.Nacionalidades, "NacionalidadId", "Descripcion");
            ViewBag.Estudios = new SelectList(repository.Estudios, "EstudioCursadoId", "Descripcion");
            ViewBag.Provincias = new SelectList(repository.Provincias, "ProvinciaID", "Nombre");
            ViewBag.GruposFamiliares = new SelectList(repository.GruposFamiliares, "GrupoFamiliarID", "Descripcion");
            ViewBag.Retorno = retorno;

            return View(postulante);
        }


        public object model { get; set; }
    }
}