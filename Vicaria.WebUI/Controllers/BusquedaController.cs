﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using Vicaria.WebUI.Models;
using System.Collections.Specialized;
using System.Diagnostics;

namespace Vicaria.WebUI.Controllers
{
    public class BusquedaController : Controller
    {

        private IBusquedasRepository repository;
        public int PageSize = 10; //tamaño de página de la grilla de postulantes

        public BusquedaController(IBusquedasRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListaBusquedas(int id, string nombre, int page = 1)
        {
            ViewBag.Categorias = new SelectList(repository.Categorias, "CategoriaId", "Descripcion");
            ViewBag.Jornadas = new SelectList(repository.JornadasLaborales, "JornadaLaboralID", "Descripcion");
            BusquedasListViewModel model = new BusquedasListViewModel
            {
                busquedas = repository.Busquedas
                    .Where(b => b.PostulanteID == id)
                    .OrderBy(b => b.BuscaDesde)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize),
                idActual = id,
                nombreActual = nombre,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Busquedas.Where(b => b.PostulanteID == id).Count()
                }
            };
            return View(model);
        }

        public ViewResult Crear(int idPostulante, string nombrePostulante)//paso el postulante dueño de la busqueda
        {
            ViewBag.Categorias = new SelectList(repository.Categorias, "CategoriaId", "Descripcion");
            ViewBag.Jornadas = new SelectList(repository.JornadasLaborales, "JornadaLaboralID", "Descripcion");
            ViewBag.Postulante = nombrePostulante;
            Busqueda busqueda = new Busqueda();
            busqueda.PostulanteID = idPostulante;
            busqueda.BuscaDesde = DateTime.Today;
            busqueda.Postulante = repository.getPostulante(idPostulante);
            busqueda.BusquedaActiva = true;
            return View("Editar", busqueda);
        }

        public ViewResult Borrar(int Id)
        {
            Busqueda busqueda = repository.Busquedas.FirstOrDefault(p => p.BusquedaID == Id);
            return View(busqueda);
        }

        [HttpPost]
        public ActionResult Borrar(Busqueda busqueda)
        {
            Busqueda busquedaBorrada = repository.BorrarBusqueda(busqueda.BusquedaID);
            if (busquedaBorrada != null)
            {
                TempData["message"] = string.Format("una búsqueda  fué borrada");
            }
            string nya = busqueda.Postulante.Apellido + " " + busqueda.Postulante.Nombre;
            return RedirectToAction("ListaBusquedas", new { id = busqueda.PostulanteID, nombre = nya, page = 1 });
        }


        public ViewResult Editar(int Id)
        {
            ViewBag.Categorias = new SelectList(repository.Categorias, "CategoriaId", "Descripcion");
            ViewBag.Jornadas = new SelectList(repository.JornadasLaborales, "JornadaLaboralID", "Descripcion");
            Busqueda busqueda = repository.Busquedas.FirstOrDefault(p => p.BusquedaID == Id);
            return View(busqueda);
        }

        [HttpPost]
        public ActionResult Editar(Busqueda busqueda)
        {
            ViewBag.Categorias = new SelectList(repository.Categorias, "CategoriaId", "Descripcion");
            ViewBag.Jornadas = new SelectList(repository.JornadasLaborales, "JornadaLaboralID", "Descripcion"); 
            Postulante postulante = repository.getPostulante(busqueda.PostulanteID);
            ViewBag.Nombre = postulante.Apellido + ", " + postulante.Nombre;
            busqueda.Categoria = repository.getCategoria(busqueda.CategoriaID);
            busqueda.JornadaLaboral = repository.getJornadaLaboral(busqueda.JornadaLaboralID);
            busqueda.Postulante = postulante;
            TryUpdateModel(busqueda);
            ModelState.Clear();
            try
            {
                ValidateModel(busqueda);
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Problema: {0} ********* Mensaje: {1}", ex.ToString(), ex.Message.ToString());
            }
            if (ModelState.IsValid)
            {
                repository.GrabarBusqueda(busqueda);
                TempData["message"] = string.Format("La búsqueda de {0} fué grabado corretamente", ViewBag.Nombre);
                return RedirectToAction("ListaBusquedas", new { id = busqueda.PostulanteID, nombre = ViewBag.Nombre });
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                foreach (var error in errors)
                {
                    
                        Trace.TraceInformation("Problema: {0} Mensaje: {1}", error.Exception, error.ErrorMessage);
                }
                //algo salio malo en os datos
                return View(busqueda);
            }
        }
    }
}