﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using Vicaria.WebUI.Models;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Diagnostics;

namespace Vicaria.WebUI.Controllers
{
    public class OfertaController : Controller
    {

        private IOfertasRepository repository;
        public int PageSize = 100; //tamaño de página de la grilla de postulantes

        public OfertaController(IOfertasRepository repo)
        {
            repository = repo;
        }

        public ActionResult ListaOfertas(int id, string nombre, int page = 1)
        {

            OfertasListViewModel model = new OfertasListViewModel
            {
                Ofertas = repository.Ofertas.Include(e=> e.Entrevistas.Select(p => p.Postulantes))
                    .Where(b => b.EmpleadorID == id)
                    .OrderBy(b => b.OfertaDesde)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize).ToList(),
                idActual = id,
                nombreActual = nombre,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Ofertas.Where(m => m.EmpleadorID == id).Count()
                }
            };
            return View(model);
        }

        

        public ActionResult BuscarOfertas(int JornadaLaboralID, string sexo, int CategoriaID, int page = 1)
        {
            
            
            OfertasListViewModel model = new OfertasListViewModel
            {
                Ofertas = repository.Ofertas.Include(e => e.Entrevistas.Select(p => p.Postulantes))
                    .Where(b => b.Sexo == sexo && b.JornadaLaboralID == JornadaLaboralID && b.CategoriaID == CategoriaID)
                    .OrderBy(b => b.OfertaDesde)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Ofertas.Include(e => e.Entrevistas.Select(p => p.Postulantes)).Where(b => b.Sexo == sexo && b.JornadaLaboralID == JornadaLaboralID && b.CategoriaID == CategoriaID).Count()
                }
            };

            ViewBag.sexo = sexo;
            ViewBag.categoria = repository.GetCategoriaDescripcion(CategoriaID);
            ViewBag.jornada = repository.GetJornadaDescripcion(JornadaLaboralID);
            return View(model);
           
        }


        public ActionResult ListaOfertasEmpleador(int idEmpleador, string nombre, int page = 1)
        {

            OfertasListViewModel model = new OfertasListViewModel
            {
                Ofertas = repository.Ofertas
                    .Where(b => b.EmpleadorID == idEmpleador)
                    .OrderBy(b => b.OfertaDesde)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize).ToList(),
                idActual = idEmpleador,
                nombreActual = nombre,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Ofertas.Where(m=> m.EmpleadorID == idEmpleador).Count()
                }
            };
            
            return View("ListaOfertas", model);
        }

        public ActionResult CrearEntrevista( int idOferta, int idBusqueda)
        {
            Entrevista entrevista = repository.GetEntrevista(idOferta, idBusqueda);
            Busqueda busqueda = repository.GetBusqueda(idBusqueda);
            if (entrevista == null)
            {
                entrevista = new Entrevista();
                entrevista.fechaAlta = DateTime.Today;
                entrevista.BusquedaID = idBusqueda;
                entrevista.Busqueda = repository.GetBusqueda(idBusqueda);
                entrevista.OfertaID = idOferta;
                entrevista.Oferta = repository.GetOferta(idOferta);
                entrevista.cubreVacante = false;
                entrevista.PostulanteId = busqueda.PostulanteID;
                repository.GrabarEntrevista(entrevista);

                //Empleador empleador = repository.getEmpleador(idEmpleador);
                TempData["message"] = string.Format("La entrevista entre {0}  y {1} fué grabada corretamente", entrevista.Oferta.Empleador.Apellido, entrevista.Busqueda.Postulante.Apellido);
            }
            else
            {
                TempData["Message"] = string.Format("La entrevista no se dió de alta por que ya existe ");
            }
            
            return RedirectToAction("Enlazar", new { id = idOferta });
            
        }
        public ViewResult Crear(int idEmpleador, string nombreEmpleador)//paso el empleador dueño de la busqueda
        {
            ViewBag.Categorias = new SelectList(repository.Categorias, "CategoriaId", "Descripcion");
            ViewBag.Jornadas = new SelectList(repository.JornadasLaborales, "JornadaLaboralID", "Descripcion");
            ViewBag.Tareas = new SelectList(repository.Tareas, "TareaID", "Descripcion");
            ViewBag.Contrataciones = new SelectList(repository.Contrataciones, "ContratacionID", "Descripcion");
            // borrar ViewBag.Edades = new SelectList(repository.Edades, "EdadId", "Rango");
            ViewBag.Estudios = new SelectList(repository.EstudiosCursados, "EstudioCursadoID", "Descripcion");
            ViewBag.Postulante = nombreEmpleador;
            Empleador empleador = repository.getEmpleador(idEmpleador);
            Oferta oferta = new Oferta();
            oferta.EmpleadorID = idEmpleador;
            oferta.Empleador = empleador;
            oferta.OfertaDesde = DateTime.Today;
            oferta.OfertaActiva = true;
            //oferta.Tareas = new List<Tarea>();
            //var tareaDefault = repository.Tareas.FirstOrDefault();
            //oferta.Tareas.Add(tareaDefault);
            //SinTareasEnOferta(oferta);

            return View("Editar", oferta);
        }

        public ActionResult Desctivar(int id)
        {
            Oferta oferta = repository.Ofertas.FirstOrDefault(p => p.OfertaID == id);
            oferta.OfertaActiva = false;
            repository.GrabarOferta(oferta);

            return RedirectToAction("ListarEntrevistasVencidas", "Entrevista");

           
        }

        public ViewResult Editar(int Id)
        {
            ViewBag.Categorias = new SelectList(repository.Categorias, "CategoriaId", "Descripcion");
            ViewBag.Jornadas = new SelectList(repository.JornadasLaborales, "JornadaLaboralID", "Descripcion");
            ViewBag.Contrataciones = new SelectList(repository.Contrataciones, "ContratacionID", "Descripcion");
            // borrar  ViewBag.Edades = new SelectList(repository.Edades, "EdadId", "Rango");
            ViewBag.Estudios = new SelectList(repository.EstudiosCursados, "EstudioCursadoID", "Descripcion");
            Oferta oferta = repository.Ofertas.FirstOrDefault(p => p.OfertaID == Id);
            //SinTareasEnOferta(oferta);
            return View(oferta);
        }

        public ViewResult Enlazar(int id)
        {
            Oferta ofertaActual = repository.GetOferta(id);
            //List<Postulante> postulantes = new List<Postulante>();  estuve cambiando el postulante por la busqueda
            List<Busqueda> busquedasEncontradas = new List<Busqueda>();
            

            var busquedas = repository.Busquedas.Where(b => b.BusquedaActiva == true && b.CategoriaID == ofertaActual.CategoriaID 
                            && b.JornadaLaboralID == ofertaActual.JornadaLaboralID
                            && b.Postulante.sexo == ofertaActual.Sexo);
            foreach(Busqueda  busqueda in busquedas)
            {
                Postulante postulante = repository.getPostulante(busqueda.PostulanteID);
                // borrar Edad edad = repository.getEdad(ofertaActual.EdadID);
                int edadMin = ofertaActual.EdadMin;
                int edadMax = ofertaActual.EdadMax;
                DateTime fechaDeHoy = DateTime.Now;
                int anioDeHoy = fechaDeHoy.Year;
                int anioDePostulante = (postulante.FechaDeNacimiento).Year;
                int edadActual = anioDeHoy - anioDePostulante;
                if (edadMin <= edadActual && edadMax >= edadActual )
                    busquedasEncontradas.Add(busqueda);
            }
            EnlacesPosiblesListViewModel model = new EnlacesPosiblesListViewModel
            {
                busquedasPosibles = busquedasEncontradas,
                oferta = ofertaActual,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = 1,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Ofertas.Where(m => m.EmpleadorID == id).Count()
                }
            };
            return View(model);
        }



        //private void SinTareasEnOferta(Oferta oferta)
        //{
        //    var todasLasTareas = repository.GetTareas;// carga todas las tareas disponibles en la base
        //    var ofertaTareas = repository.TareasEnOferta(oferta); //carga las tareas de la oferta corriente
        //    var viewModel = new List<TareasDeOfertaListViewModel>();
        //    foreach (var tarea in todasLasTareas)
        //    {
        //        viewModel.Add(new TareasDeOfertaListViewModel
        //        {
        //            TareaID = tarea.TareaID,
        //            Descripcion = tarea.Descripcion,
        //            Asignada = ofertaTareas.Contains(tarea.TareaID)
        //        });
        //    }
        //    ViewBag.Tareas = viewModel; //en este tambin va marcad si esta o no seleccionada
        //}

        //private void LlenarTareasEnOferta(Oferta oferta, string[] TareasSeleccionadas)
        //{
        //    //var todasLasTareas = repository.GetTareas;// carga todas las tareas disponibles en la base
        //    //var ofertaTareas = repository.TareasEnOferta(oferta); //carga las tareas de la oferta corriente
        //    var viewModel = new List<TareasDeOfertaListViewModel>();
        //    List<Tarea> coleccionTareas = new List<Tarea>();//va a guardar las tareas actualizadas
        //    //Verifico si la tarea en un alta
        //    if (oferta.OfertaID == 0)
        //    {
        //        //tengo una oferta nueva y le actualizo las tareas seleccionadas en la creación
        //        if (TareasSeleccionadas == null)
        //        {
        //            oferta.Tareas = new List<Tarea>();
        //        }
        //        var tareasSeleccionadasHS = new HashSet<string>(TareasSeleccionadas); //tareas ingresadas
        //        var ofertaTarea = new HashSet<int>();
        //        if (oferta.Tareas == null) //verifico si la oferta tiene tareas cargada
        //        {
        //            ofertaTarea = new HashSet<int>();
        //        }
        //        else
        //        {
        //            ofertaTarea = new HashSet<int>(oferta.Tareas.Select(t => t.TareaID)); //cargo las tareas existentes
        //        }
        //        foreach (var tarea in todasLasTareas) //recorro todas las tareas disponibles en la base
        //        {
        //            if (tareasSeleccionadasHS.Contains(tarea.TareaID.ToString())) //verifico si la tarea de la base fue seleccionada
        //            {
        //                if (!ofertaTarea.Contains(tarea.TareaID)) //si no esta la agrega
        //                {
        //                    coleccionTareas.Add(tarea);
        //                }
        //            }
        //            else
        //            {
        //                if (ofertaTarea.Contains(tarea.TareaID)) //si esta la quito
        //                {
        //                    coleccionTareas.Remove(tarea);
        //                }
        //            }
        //        }
        //        oferta.Tareas = coleccionTareas;
        //    }
        //    else
        //    {
        //        //Actualizo las tareas
        //        if (oferta != null)
        //        {
        //            var tareasSeleccionadasHS = new HashSet<string>();
        //            //tengo una oferta ya cargada con tareas y las actualizo
        //            if (TareasSeleccionadas == null) //verifico si la oferta tiene tareas cargada
        //            {
        //                oferta.Tareas = new List<Tarea>();
        //            }
        //            else
        //            {
        //                tareasSeleccionadasHS = new HashSet<string>(TareasSeleccionadas); //asigno tareas ingresadas
        //            }
        //            var ofertaTarea = new HashSet<int>();//guarda las ofertas que vienen d la base
        //            if (oferta.Tareas == null)
        //            {
        //                ofertaTarea = new HashSet<int>();
        //            }
        //            else
        //            {
        //                ofertaTarea = new HashSet<int>(oferta.Tareas.Select(t => t.TareaID));//Cargo las tareas existentes
        //            }

        //            foreach (var tarea in todasLasTareas)
        //            {
        //                if (tareasSeleccionadasHS.Contains(tarea.TareaID.ToString()))
        //                {
        //                    if (!ofertaTarea.Contains(tarea.TareaID))
        //                    {
        //                        coleccionTareas.Add(tarea);
        //                    }
        //                }
        //                else
        //                {
        //                    if (ofertaTarea.Contains(tarea.TareaID))
        //                    {
        //                        coleccionTareas.Remove(tarea);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    oferta.Tareas = coleccionTareas;
        //    //fin de actualizacion de tareas-------------------------------------------

        //    foreach (var tarea in coleccionTareas)
        //    {
        //        viewModel.Add(new TareasDeOfertaListViewModel
        //        {
        //            TareaID = tarea.TareaID,
        //            Descripcion = tarea.Descripcion,
        //            Asignada = true
        //        });
        //    }
        //    ViewBag.Tareas = viewModel; //en este tambin va marcad si esta o no seleccionada
        //}





        [HttpPost]
        public ActionResult Editar(Oferta oferta, string[] TareasSeleccionadas = null) //
        {
            ViewBag.Categorias = new SelectList(repository.Categorias, "CategoriaId", "Descripcion");
            ViewBag.Jornadas = new SelectList(repository.JornadasLaborales, "JornadaLaboralID", "Descripcion");
            ViewBag.Contrataciones = new SelectList(repository.Contrataciones, "ContratacionID", "Descripcion");
            // borrar ViewBag.Edades = new SelectList(repository.Edades, "EdadId", "Rango");
            ViewBag.Estudios = new SelectList(repository.EstudiosCursados, "EstudioCursadoID", "Descripcion");
            EstudioCursado estudio = repository.getEstudioCursado(oferta.EstudioCursadoID);
            oferta.EstudioCursado = estudio;
            Empleador empleador = repository.getEmpleador(oferta.EmpleadorID);
            oferta.Empleador = empleador;
            oferta.Categoria = repository.getCategoria(oferta.CategoriaID);
            
            if (TareasSeleccionadas == null)
            {
                //SinTareasEnOferta(oferta);
            }
            else
            {
                //LlenarTareasEnOferta(oferta, TareasSeleccionadas);
            }

            TryUpdateModel(oferta);
            ModelState.Clear();
            try
            {
                ValidateModel(oferta);
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Problema: {0} ********* Mensaje: {1}", ex.ToString(), ex.Message.ToString());
            }
            
            if (ModelState.IsValid)
            {
            repository.GrabarOferta(oferta);
            TempData["message"] = string.Format("La oferta de {0} fué grabado corretamente", empleador.Apellido);
            return RedirectToAction("ListaOfertas", new { id = oferta.EmpleadorID, nombre = empleador.Apellido + ", " + empleador.Nombre });
                //TODO: tengo qeu ver como le paso los parametros a listabusqeuda
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                foreach (var error in errors)
                {

                    Trace.TraceInformation("Problema: {0} ********* Mensaje: {1}", error.Exception, error.ErrorMessage);
                }
                //algo salio malo en os datos
                return View(oferta);
            }
        }
    }
}