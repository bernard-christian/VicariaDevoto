﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicaria.WebUI.Models;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using System.Diagnostics;

namespace Vicaria.WebUI.Controllers
{
    public class EntrevistaController : Controller
    {
       private IEntrevistaRepository repository;
       public int PageSize = 100;

       public EntrevistaController(IEntrevistaRepository repo)
        {
            repository = repo;
        }

        public ViewResult ListarEntrevistas(int page = 1)
        {

            EntrevistasListViewModel model = new EntrevistasListViewModel
            {
                entrevistas = repository.Entrevistas
                    .OrderBy(x => x.fechaEntrevista).ThenBy(x => x.Oferta.Empleador.Apellido)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Entrevistas.Count()
                }
            };
            return View(model);
        }

        public ViewResult ListarEntrevistasVencidas(int page = 1)
        {
            DateTime fechaLimite = DateTime.Today.AddDays(-10);
            var fechaNula = new DateTime(0001, 01, 01);
            EntrevistasListViewModel model = new EntrevistasListViewModel
            {
                
            entrevistas = repository.Entrevistas
                    .Where(x => x.fechaAlta < fechaLimite && x.Oferta.OfertaActiva)
                    .OrderBy(x => x.OfertaID)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Entrevistas.Count()
                }
            };
            return View(model);
        }

        public ActionResult ListarEntrevistasDeEmpleador(int idEmpleador, int page = 1)
        {
            EntrevistasListViewModel model = new EntrevistasListViewModel
            {
                entrevistas = repository.Entrevistas
                    .Where(e => e.Oferta.EmpleadorID == idEmpleador)
                    .OrderBy(e => e.fechaAlta)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Entrevistas.Where(e => e.Oferta.EmpleadorID == idEmpleador).Count()
                }
            };
            return View(model);
        }

        public ActionResult ListarEntrevistasDePostulante(int idPostulante, int page = 1)
        {
            EntrevistasListViewModel model = new EntrevistasListViewModel
            {
                entrevistas = repository.Entrevistas
                    .Where(e => e.Busqueda.PostulanteID == idPostulante)
                    .OrderBy(e => e.fechaAlta)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Entrevistas.Where(e => e.Busqueda.PostulanteID == idPostulante).Count()
                }
            };
            return View(model);
        }


        public ActionResult Aprobar(int? id)
        {
            Entrevista entrevista = repository.Entrevistas.FirstOrDefault(p => p.entrevistaID == id);
            entrevista.cubreVacante = true;
            if (ModelState.IsValid)
            {
                repository.GrabarEntrevista(entrevista);
                TempData["message"] = string.Format("{0} fué actualizado corretamente", entrevista.Oferta.Empleador.Apellido);
                return RedirectToAction("ListarEntrevistasVencidas");
            }
            else
            {
                //algo salio malo en os datos
                return View("ListarEntrevistasVencidas");
            }
        }

        public ActionResult Desaprobar(int? id)
        {
            Entrevista entrevista = repository.Entrevistas.FirstOrDefault(p => p.entrevistaID == id);
            entrevista.cubreVacante = false;
            if (ModelState.IsValid)
            {
                repository.GrabarEntrevista(entrevista);
                TempData["message"] = string.Format("{0} fué actualizado corretamente", entrevista.Oferta.Empleador.Apellido);
                return RedirectToAction("ListarEntrevistasVencidas");
            }
            else
            {
                //algo salio malo en os datos
                return View("ListarEntrevistasVencidas");
            }
        }

        public ViewResult Editar(int Id)
        {
            Entrevista entrevista = repository.Entrevistas.FirstOrDefault(x => x.entrevistaID == Id);
            return View(entrevista);
        }

        [HttpPost]
        public ActionResult Editar(Entrevista entrevista)
        {
            Oferta oferta = repository.GetOferta(entrevista.OfertaID);
            Busqueda busqueda = repository.GetBusqueda(entrevista.BusquedaID);
            entrevista.Oferta = oferta;
            entrevista.Busqueda = busqueda;
            TryUpdateModel(entrevista);
            ModelState.Clear();
            try
            {
                ValidateModel(entrevista);
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Problema: {0} ********* Mensaje: {1}", ex.ToString(), ex.Message.ToString());
            }
            if (ModelState.IsValid)
            {
                if (entrevista.cubreVacante )
                {
                    //tengo que dar de bajala oferta y la busqueda
                    oferta.Vacantes = oferta.Vacantes - 1;
                    if (oferta.Vacantes <= 0)
                    {
                         try
                        {
                            ValidateModel(oferta);
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceInformation("Problema: {0} ********* Mensaje: {1}", ex.ToString(), ex.Message.ToString());
                        }
                         if (ModelState.IsValid)
                         {
                             oferta.OfertaActiva = false;
                             repository.GrabarOferta(oferta);
                         }
                    }
                    
                    busqueda.BusquedaActiva = false;
                    repository.GrabarBusqueda(busqueda);
                }
                repository.GrabarEntrevista(entrevista);
                return RedirectToAction("ListarEntrevistas");
            }
            else
            {
                //alg salio malo en os datos
                return View(entrevista);
            }
        }

        public ViewResult Crear(int idPostulante, int idEmpleador, int idOferta)
        {
            Entrevista entrevista = new Entrevista();
            entrevista.fechaAlta = DateTime.Today;
            //entrevista.EmpleadorID = idEmpleador;
            //entrevista.PostulanteID = idPostulante;
            entrevista.cubreVacante = false;
            repository.GrabarEntrevista(entrevista);
            

            return View("EditarEntrevista");
        }

        public ViewResult BorrarEntrevista(int Id)
        {
            Entrevista entrevista = repository.Entrevistas.FirstOrDefault(p => p.entrevistaID == Id);
            return View(entrevista);
        }

        [HttpPost]
        public ActionResult BorrarEntrevista(Entrevista entrevista)
        {
            Entrevista entrevistaBorrada = repository.BorrarEntrevista(entrevista.entrevistaID);
            if (entrevistaBorrada != null)
            {
                TempData["message"] = string.Format("Una entrevista con Empleador {0} y postulante {1} fué borrada", entrevistaBorrada.Oferta.Empleador.Apellido, entrevistaBorrada.Busqueda.Postulante.Apellido);
            }
            return RedirectToAction("ListarEntrevistas");
        }
    }
}