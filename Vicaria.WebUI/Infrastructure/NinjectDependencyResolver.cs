﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using Moq;
using Vicaria.Domain.Abstract;
using Vicaria.Domain.Entities;
using Vicaria.Domain.Concrete;

namespace Vicaria.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type ServiceType)
        {
            return kernel.TryGet(ServiceType);
        }


        public IEnumerable<object> GetServices(Type ServiceType)
        {
            return kernel.GetAll(ServiceType);
        }

        private void AddBindings()
        {
            //le digo a Ninject que el request de IPostulanteRepositori brinda un EFPostulanteRepository
            kernel.Bind<IPostulanteRepository>().To<EFPosulanteRepository>();
            kernel.Bind<IAdminRepository>().To<EFAdminRepository>();
            kernel.Bind<IEstudiosRepository>().To<EFEstudiosRepository>();
            kernel.Bind<INacionalidadesRepository>().To<EFNacionalidadesRepository>();
            kernel.Bind<IProvinciasRepository>().To<EFProvinciasRepository>();
            kernel.Bind<IDocumentosRepository>().To<EFDocumentosRepository>();
            kernel.Bind<IGrupoFamiliarRepository>().To<EFGrupoFamiliarRepository>();
            kernel.Bind<ICategoriasRepository>().To<EFCategoriasRepository>();
            kernel.Bind<IJornadasLaboralesRepository>().To<EFJornadasLaboralesRepository>();
            kernel.Bind<IBusquedasRepository>().To<EFBusquedasRepository>();
            kernel.Bind<IEmpleadorRepository>().To<EFEmpleadorRepository>();
            kernel.Bind<IEdadRepository>().To<EFEdadesRepository>();
            kernel.Bind<IContratacionesRepository>().To<EFContratacionesRepository>();
            kernel.Bind<IOfertasRepository>().To<EFOfertasRepository>();
            kernel.Bind<IEntrevistaRepository>().To<EFEntrevistasRepository>();

            //EmailSettings emailSettings = new EmailSettings
            //{
            //    WriteAsFile = bool.Parse(ConfigurationSettings
            //        .AppSettings["Email.WriteAsFile"] ?? "false")
            //};
            //kernel.Bind<IOrderProcessor>().To<EmailOderProcessor>()
            //    .WithConstructorArgument("settings", emailSettings);
        }
    }


}