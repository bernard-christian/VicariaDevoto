﻿CREATE TABLE [dbo].[Table]
(
	[PostulanteId] INT NOT NULL PRIMARY KEY, 
    [Nombre] VARCHAR(50) NOT NULL, 
    [Apellido] VARCHAR(50) NOT NULL, 
    [Documento] INT NOT NULL, 
    [Telefono] VARCHAR(50) NULL, 
    [Busqueda] VARCHAR(MAX) NULL
)
